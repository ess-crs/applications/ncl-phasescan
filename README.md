# NCL Phase Scan Application

Using REMI for a graphical interface

Alternative CLI interface which can conveniently be used through Jupyter

## Installation

`pip install git+https://gitlab.esss.lu.se/ess-crs/applications/ncl-phasescan.git`

## Usage

### GUI (deprecated)

Graphical user interface is currently based on REMI. To start it execute

 	python phaseScanGUI.py

### Script/command line/jupyter
 
For the script interface, this can be used in Jupyter once you have the package installed. See example usage in `phaseScanCLI.py` and `phaseScanRFQSimplest.py`.


### API documentation

See [https://confluence.esss.lu.se/display/BPCRS/CLI+User+Documentation](https://confluence.esss.lu.se/display/BPCRS/CLI+User+Documentation)

## Development

Development is welcome, see gitlab page for details. Discussion on this topic is welcome as well
