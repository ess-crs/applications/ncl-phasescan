CLI User Documentation
======================

The phase scan application has a command line interface, where the main functions are described below. As this is implemented in Python, in principle all functions in the library can be reached.

The main functions that are intended to be used (and intended to be sufficient) are found in the class PhaseScan in phasescan.py.  This class is also documented below.

A minimal example is found at the bottom of this page. More complete scan examples can be found in the files phaseScanNNN.py, except the phaseScanGUI.py which starts up the graphical user interface (GUI) which is implemented in REMI.

The sources for this application are found on `GitLab`_.

.. _GitLab: https://gitlab.esss.lu.se/ess-crs/applications/ncl-phasescan

.. contents:: Table of Contents
   :depth: 3
   :local:

.. automodule:: phasescan.phasescan


Scan configuration
*******************

There are several properties and functions to configure the scan.

Initialize
----------

To set the sequence and accelerator used for this scan, we have the following functions

.. autoclass:: PhaseScan
   :class-doc-from: init
   :members: set_sequence, set_accelerator, get_all_sequences


Strategies
----------

One of the fundamental design considerations is that each section of the machine may end up with a rather different phase and amplitude matching method, that may or may not depend on simulated curves. Hence a separate strategy is written for each, but the user is free to mix and match as one finds sensible.

Here we list some of the strategies under development so far

Default
.......
.. automodule:: phasescan.strategies.default

.. autoclass:: DefaultStrategy

MEBT Bunchers
.............

.. automodule:: phasescan.strategies.mebt

.. autoclass:: MebtClosedStrategy

.. autoclass:: MebtOpenStrategy

DTL
...

.. automodule:: phasescan.strategies.dtl

.. autoclass:: DtlClosedStrategy

.. automodule:: phasescan.strategies.dtl2

.. autoclass:: DtlClosedStrategy

.. autoclass:: DtlOpenStrategy


.. automodule:: phasescan.phasescan
   :noindex:


Scan range
----------

We list below the properties that can be modified to fine tune the scan area

.. autoclass:: PhaseScan
    :noindex:
    :members: amplmax, amplmin, amplinterval, phasemax, phasemin, phaseinterval, fixed_phase, tdelta, set_relative_phases, set_relative_amplitudes


Device selection
----------------

Then, there are a few functions to set the devices used during the scan

.. autoclass:: PhaseScan
    :noindex:
    :members: add_cavity, remove_cavity, add_monitor, remove_monitor, get_all_nodes_of_type, include_waveforms, downstream_off, upstream_off, skip_upstream_bpms


Simulate, run and apply
-----------------------

Finally, the functions used to simulate, run scan, analyse and apply settings etc are

.. autoclass:: PhaseScan
    :noindex:
    :members: run_simulation, load_simulation, run_scan, load_scan, save_scan, analyse, apply_settings, get_new_settings

Debugging
---------

Debugging the logic can sometimes be useful. For that we have a couple of functions listed below.

.. autoclass:: PhaseScan
    :noindex:
    :members: debug_no_scanning, debug_ignore_nobeam, debug_plot_analysis


Minimal Example
***************

When you select the sequence, by default all RF devices in that section is selected,
with a default scan range in phase, and only the current set point in amplitude.
All BPM's are selected to be aqcuired at every scan point.

A minimal example, which simulates, runs a scan and applies corrections, therefore looks like::

    import oxal
    from phasescan import phasescan

    oxal.startJVM()
    sim = phasescan.PhaseScan(oxal)
    sim.set_sequence("MEBT")

    sim.run_simulation()

    sim.run_scan()
    sim.analyse()
    sim.apply_settings(sim.all_cavities)

