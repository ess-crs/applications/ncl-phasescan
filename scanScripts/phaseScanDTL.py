import phasescan
import oxal
from strategies.dtl import DtlStrategy


def scan_cavity(sim, index, parameter):
    for i in range(1, 6):  # Generally this should just be loop over cavity elements
        if i == index:
            print(f"Add DTLTank{i}")
            sim.add_cavity(f"DTLTank{i}")
        else:
            print(f"Remove DTLTank{i}")
            sim.remove_cavity(f"DTLTank{i}")
    sim.run_scan()
    sim.analyse()
    sim.apply_settings(sim.all_cavities, parameters=[parameter])


def main(sequence):
    """
    Run a scan using a script, ie skip UI all together
    """

    oxal.startJVM()
    sim = phasescan.PhaseScan(oxal, strategy=DtlStrategy)
    sim.set_sequence(sequence)

    # By default, all cavities and all BPM's are added to the scan

    # First we fit phase of each cavity:
    sim.phasemin = -180
    sim.phasemax = 0
    sim.phaseinterval = 10

    # Run simulation and scan:
    sim.run_simulation()

    scan_cavity(sim, 1, "Phase")


if __name__ == "__main__":
    main("DTL")
