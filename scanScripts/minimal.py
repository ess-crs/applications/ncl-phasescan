from phasescan import phasescan
import oxal

oxal.startJVM()
sim = phasescan.PhaseScan(oxal)
sim.set_sequence("DTL")

sim.debug_ignore_nobeam = True

sim.run_simulation()

sim.run_scan()
# sim.analyse()
# sim.apply_settings(sim.all_cavities)
