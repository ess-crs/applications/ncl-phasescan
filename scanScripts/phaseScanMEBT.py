from phasescan import phasescan
from phasescan.strategies.mebt import MebtOpenStrategy
import oxal
import os

SHIFT_FOLDER = "/nfs/Linacshare_controlroom/Commissioning/SRR2b/20220602A/"
FNAME = os.path.join(SHIFT_FOLDER, "TEST_MEBT_B1_Scan_PP_3.h5")

SCAN = True


if SCAN:
    oxal.startJVM()
    sim = phasescan.PhaseScan(oxal, strategy=MebtOpenStrategy)

    # !ls ~/notebooks/openxal-lattice/current/main.xal

    # sim.set_accelerator("/opt/OpenXAL/optics/current/main.xal")
    # sim.set_accelerator("/home/yngvelevinsen/notebooks/openxal-lattice/current/main.xal")
    # sim.set_accelerator("/home/yngvelevinsen/Programming/physapps/openxal-lattice/current/main.xal")
    sim.set_accelerator("/home/operator-mcr/BeamPhysics/openxal-lattice/current/main.xal")
    sim.set_sequence("MEBT", select_all_cavities=False, select_all_monitors=True)

    sim.add_cavity("MEBT-010:EMR-Cav-001")
    sim.tdelta = 2

    # +
    # Run simulation: (not working for RFQ)
    sim.run_simulation()
    # -

    # First we fit phase of each cavity:
    sim.phasemin = -90 - 180
    sim.phasemax = -90 + 180
    sim.phaseinterval = 5

    sim.amplmin = 0.95
    sim.amplmax = 1.0
    sim.amplinterval = 0.05

    # sim.debug_no_scanning = True
    sim.debug_plot_analysis = False  # !! This breaks saving !!
    sim.run_scan()

    sim.analyse()
    print(sim.get_new_settings())

    sim.save_scan(FNAME)

from matplotlib import pyplot as plt
import h5py
import numpy as np

# -

CAVITY = "MEBT-010:EMR-Cav-001"
BPM = "MEBT-010:PBI-BPM-002"

data = h5py.File(FNAME, "r")

phases = data["entry"]["data"]["phases"][:]

print(data["entry"].keys())
print(data["entry"][CAVITY].keys())

plt.figure(figsize=(15, 12))
for bpm in data["entry"][CAVITY]:
    if bpm == "set_points":
        print(bpm, data["entry"][CAVITY][bpm][:])
        continue
    resp = data["entry"][CAVITY][bpm]["phases"][0]
    resp -= np.mean(resp)
    plt.plot(phases + 90, resp, label=bpm)
plt.legend()
plt.xlabel("MEBT Buncher 1 Phase [rad]")
plt.ylabel("BPM Phase [deg]")
plt.show()

print("FINITO")
