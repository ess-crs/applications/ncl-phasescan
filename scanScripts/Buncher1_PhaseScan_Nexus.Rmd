---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```{python}
# %matplotlib inline
from matplotlib import pyplot as plt
import epics
import os
```

```{python}
import essnexus
print(essnexus.__version__)
print(essnexus.__file__)
```

# Define Scanning and Saving PVs

```{python}
PVscanSet1='MEBT-010:RFS-DIG-101:SPTbl-Ang'     
PVscanSet2='MEBT-010:RFS-DIG-101:SPTbl-TblToFW'       

PVscanGet1='MEBT-010:RFS-DIG-101:SPTbl-Ang-RB.PROC'
PVscanGet2='MEBT-010:RFS-DIG-101:SPTbl-Ang-RB'


PVtomonitor = ['MEBT-010:PBI-BPM-001:SA-TR1-ArrayData',
               'MEBT-010:PBI-BPM-002:SA-TR1-ArrayData',
               'MEBT-010:PBI-BPM-003:SA-TR1-ArrayData',
               'MEBT-010:PBI-BPM-005:SA-TR1-ArrayData',
               'MEBT-010:RFS-DIG-101:SPTbl-Ang-RB',
               'MEBT-010:RFS-DIG-101:SPTbl-Mag-RB'  ]

shiftID = 'NSO:Ops:SID'
```

```{python}
# scan kicker from startvalue - to final value in stepvalue
EPICSPVs=[]

def ConnectPVs():
    global EPICSPVs
    okPV = True   
    try:
        print(PVscanSet1, epics.caget(PVscanSet1))
        okPV = (okPV and True)
    except:
        okPV = False
        print('{} not connected \n',PVscanSet1)
        return okPV
    try:
        print(PVscanSet2, epics.caget(PVscanSet2))
        okPV = (okPV and True)
    except:
        okPV = False
        print('{} not connected \n',PVscanSet2)
        return okPV
    try:
        print(PVscanGet1, epics.caget(PVscanGet1))
        okPV = (okPV and True)
    except:
        okPV = False
        print('{} not connected \n',PVscanGet1)
        return okPV
    try:
        print(PVscanGet2, epics.caget(PVscanGet2))
        okPV = (okPV and True)
    except:
        okPV = False
        print('{} not connected \n',PVscanGet2)
        return okPV
    for PV in PVtomonitor:
        EPICSPVs.append(epics.PV(PV, auto_monitor=True))
        try:
            print(PV, epics.caget(PV))
            okPV = (okPV and True)
        except:
            okPV = False
            print('{} not connected \n'.format(PV))
            return okPV
    print('All PVs connected \n')
    return okPV
    
def DisconnectPVs(): 
    global EPICSPVs
    for PV in EPICSPVs:
        PV.clear_callbacks()
        PV.clear_auto_monitor()
        EPICSPVs.remove(PV)
    print("All PVs disconected")
    
def move(toValue, setPV1, setPV2, checkPV1, checkPV2,N):
    epics.caput(setPV1, toValue*np.ones(N))
    epics.caput(setPV2, 1)
    time.sleep(1)
    epics.caput(checkPV1,1)
    val = abs(np.mean(epics.caget(PVscanGet1)-toValue*np.ones(N)))>0.1
    print('Moved ', val)
        
def scanPV(startValue, finalValue, stepValue, Nsamples):
    path = "/shares/controlroom/Commissioning/SRR2b/"+epics.caget(shiftID)+'/'
    if not os.path.isfile(path+filename):
        ok_to_go = ConnectPVs()
        if ok_to_go:
            outfile = essnexus.File(path+filename)
            outfile.create_file_entry(run_cycle=epics.caget(shiftID),title=scan_title,description=scan_description)
            outfile.create_file_user(name=scan_user, role='', affiliation='ESS', facility_user_id='ESS')            
            print("------------------------------- \n")
            print("Intiating scan: \n")
            N = len(epics.caget(PVscanSet1))
            for value in arange(startValue, finalValue, stepValue):
                print("Scanning value",value)
                move(value, PVscanSet1, PVscanSet2, PVscanGet1, PVscanGet2, N)
                time.sleep(1)
                for iteration in range(Nsamples):
                    for PV in EPICSPVs:
                        outfile.add_dataset(PVscanSet1+"_"+str(value),PV.pvname+'_sample_'+str(iteration),array(PV.value))
                    time.sleep(1)
            print('done')
            outfile.close()
        DisconnectPVs()
    else:
        print('File already exists!')
```

```{python}
#Create file Meatadata before scanning
filename = 'Buncher1_PhaseScan_withBeam_80kV.hdf5'

scan_title = 'MEBT buncher 1 phase Scan'
scan_description = 'Trying phase scan of buncher for the first time'
scan_user = 'Natalia'
```

```{python}
#Define scanning limits

startValue = -np.pi
finalValue = np.pi+np.pi/20
stepValue = np.pi/20

#Define number fo measurements:

Nsamples = 10
```

```{python}
#Start Scanning
scanPV(startValue, finalValue, stepValue, Nsamples)
```

```{python}
DisconnectPVs() 
```

```{python}
N = len(epics.caget(PVscanSet1))
move(0, PVscanSet1, PVscanSet2, PVscanGet1, PVscanGet2, N)
```

```{python}

```
