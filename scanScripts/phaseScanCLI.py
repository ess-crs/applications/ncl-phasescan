# +
from phasescan import phasescan
import oxal

print(phasescan.__file__)


# -


def scan_cavity(sim, cavity, all_cavities, parameter):
    """
    Simple macro to scan one cavity (give index) and set
    one parameter of that cavity
    """
    for c in all_cavities:
        if c == cavity:
            sim.add_cavity(c)
        else:
            sim.remove_cavity(c)
    sim.run_scan()
    # sim.analyse()
    # sim.apply_settings(sim.all_cavities, parameters=[parameter])


oxal.startJVM()
sim = phasescan.PhaseScan(oxal)

# +
sequence = "MEBT"
# sim.set_accelerator("/path/to/openxal-lattice/current/main.xal")
# By default, all cavities and all BPM's are added to the scan
sim.set_sequence(sequence)

cavities = [str(c) for c in sim.all_cavities]
# -

cavities

# Run simulation:
sim.run_simulation()

sim.debug_ignore_nobeam = True
sim.debug_test_mode = True

sim._simulator._model._accelerator.channelSuite().getChannelFactory().isTest()

# First we fit phase of each cavity:
sim.phasemin = -180
sim.phasemax = 0
sim.phaseinterval = 10
sim.tdelta = 1
for cav in cavities:
    if "MEBT" in cav:
        scan_cavity(sim, cav, cavities, "Phase")
        break

# +
# Now we want to match amplitude of cavity:
sim.phasmin = -120
sim.phasemax = -60
sim.phaseinterval = 5
sim.amplmin = 0.9
sim.amplmax = 1.1
sim.amplinterval = 0.02
# sim._analyser.strategy.dbg_plot = True
sim._analyser.strategy.fixed_phase = True  # Do not try to match phase anymore, assume perfect

for cav in cavities:
    scan_cavity(sim, cav, cavities, "Amplitude")

# +
# Now we scan amplitude once more
# (needed if initial amplitude error is too large for the range we scan)
sim.phasemin = -120
sim.phasemax = -60
sim.phaseinterval = 5
sim.amplmin = 0.98
sim.amplmax = 1.02
sim.amplinterval = 0.002
# sim._analyser.strategy.dbg_plot = True
sim._analyser.strategy.fixed_phase = True  # Do not try to match phase anymore, assume perfect

for cav in cavities:
    scan_cavity(sim, cav, cavities, "Amplitude")
