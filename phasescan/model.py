import jpype

# TODO This class can maybe be rather put in our oxal library when it is more mature?

XAL_DM_STR = "xal.smf.data.XMLDataManager"
XAL_AF_STR = "xal.sim.scenario.AlgorithmFactory"
XAL_PF_STR = "xal.sim.scenario.ProbeFactory"
XAL_SC_STR = "xal.sim.scenario.Scenario"


class Model:
    def __init__(self, oxal):
        self._oxal = oxal
        self._accelerator = None
        self._accelerator_path = None
        self._sequence = None
        self._test_mode = False
        self.probe = None
        self.model = None

    @property
    def test_mode(self):
        # self._accelerator.channelSuite().getChannelFactory().isTest()
        return self._test_mode

    @test_mode.setter
    def test_mode(self, test_mode):
        self._accelerator.channelSuite().getChannelFactory().setTest(test_mode)
        self._test_mode = self._accelerator.channelSuite().getChannelFactory().isTest()

    def get_test_suffix(self):
        if self.test_mode:
            return self._accelerator.channelSuite().getChannelFactory().getTestSuffix()
        else:
            return ""

    @property
    def accelerator(self):
        return self._accelerator

    @accelerator.setter
    def accelerator(self, path):
        if path is None:
            path = self._oxal.getDefaultPath()
        self._accelerator_path = path
        self._accelerator = self._oxal.loadAccelerator(self._accelerator_path)
        self._accelerator.channelSuite().getChannelFactory().setTest(self._test_mode)

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, sequence):
        if self._accelerator is None:
            self._accelerator = self._oxal.loadAccelerator()
            self._accelerator.channelSuite().getChannelFactory().setTest(self._test_mode)
        if isinstance(sequence, str):
            self._sequence = self.accelerator.getSequence(sequence)
            if not self._sequence:  # assume combo-seq
                self._sequence = self.accelerator.getComboSequence(sequence)
                if not self._sequence:
                    raise ValueError(f"Sequence {sequence} not found")
        else:
            self._sequence = sequence
        pass

    def get_accelerator_path(self):
        return self._accelerator_path

    def get_probe(self):
        envelope_tracker = jpype.JClass(XAL_AF_STR).createEnvelopeTracker(self.sequence)
        # envelope_tracker.setMaxIterations(1000)
        # envelope_tracker.setAccuracyOrder(1)
        # envelope_tracker.setErrorTolerance(0.001)

        probe_factory = jpype.JClass(XAL_PF_STR)
        self.probe = probe_factory.getEnvelopeProbe(self.sequence, envelope_tracker)
        return self.probe

    def get_model(self, probe=None, sync_mode="DESIGN"):
        scenario = jpype.JClass(XAL_SC_STR).newScenarioFor(self.sequence)
        if probe:
            self.probe = probe
        self.model = scenario.newScenarioFor(self._sequence)
        if self.probe:
            self.model.setProbe(self.probe)
        self.model.setSynchronizationMode(sync_mode)
        return self.model

    def get_trajectory_from_model(self, model=None):
        if model:
            self.model = model
        if not self.model:
            raise ValueError("Cannot call this function without a model defined")
        self.probe = self.get_probe()
        self.model.setProbe(self.probe)
        self.model.resync()
        self.model.run()
        self.probe = self.model.getProbe()
        return self.probe.getTrajectory()
