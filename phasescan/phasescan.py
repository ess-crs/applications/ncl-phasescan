import time
import numpy
import numbers
import sys
import threading
import traceback
from . import scan_config
from . import myepics
from . import backend
from . import io

"""
.. module:: phasescan
   :platform: Unix
   :synopsis: Main module of this package

.. moduleauthor:: Yngve Levinsen <Yngve.Levinsen@ess.eu>
"""


class PhaseScan:
    def __init__(self, oxal, **kwargs):
        """
        The main class of this module. Start by initializing this

        :param oxal: The OpenXAL module, must have been imported first
        :param strategy: Strategy class to use, or None for default
        """
        self.all_monitors = []
        self.all_cavities = []
        self.all_pvs = []
        self._config = scan_config.ScanConfig()
        self._backend = backend.Phaser(oxal, self._config, **kwargs)
        self._io = io.IO(self._backend)
        self._step_counter = 0
        self.skip_upstream_bpms = True  #: If True, do not record upstream BPM data even if listed (Default True)
        self.default_settings = {}
        self.use_bpms = {}
        self.scan_ongoing = False
        self.scan_halted = False
        self.scan_status_receiver = print  # For messages during a (threaded) scan
        self.log = print  # For messages outside of a scan
        self.debug_no_scanning = False  #: if True, do not actually touch anything (Default False)
        self.debug_ignore_nobeam = False  #: if True, ignore the absence of beam (Default False)
        self.debug_plot_analysis = False  #: if True, create a simple plot after analysis and show (Default False)
        self.debug_plot_during_scan = False  #: if True, plot the phases during the scan
        self.immediate_analysis = False  #: if True, run analysis after each cavity and go to new settings
        self.set_relative_phases = (
            True  #: if True, set phases as if 0 is "design", and assume set point before scan is best guess (Default True)
        )
        self.set_relative_amplitudes = (
            True  #: if True, set amplitude as if 1.0 is set point before scan begins, Otherwise amp values in MV (Default True)
        )
        self.set_accelerator()

    def save_config(self, filename):
        """
        Save a config file with scan settings

        :param filename: name of config file to load
        """
        self._io.save_config(filename, self._config)

    def load_config(self, filename):
        """
        Load a config file with scan settings

        :param filename: name of config file to load
        """
        self._config = self._io.load_config(filename)

    @property
    def filepath(self):
        return self._io.get_filepath()

    def set_filepath(self, filepath, overwrite=False):
        self._io.set_filepath(filepath, overwrite)

    def close_file(self, force=False):
        if self.scan_ongoing and not force:
            raise ValueError("Scan ongoing/halted, not closing file")
        self._io.close_file()

    @property
    def downstream_off(self):
        """
        If True, detune downstream cavities of the one being modulated (Default False)
        """
        return self._config.downstream_off

    @downstream_off.setter
    def downstream_off(self, is_off):
        self._config.downstream_off = is_off

    @property
    def upstream_off(self):
        """
        If True, detune downstream cavities of the one being modulated (Default False)
        """
        return self._config.upstream_off

    @upstream_off.setter
    def upstream_off(self, is_off):
        self._config.upstream_off = is_off

    @property
    def fixed_phase(self):
        """If True, do not scan phase (Default False)"""
        return self._backend.analyser.strategy.fixed_phase

    @fixed_phase.setter
    def fixed_phase(self, value):
        self._backend.analyser.strategy.fixed_phase = value

    @property
    def phasemin(self):
        """
        The minimum value for the phase range in degrees, where 0 equals current setting

        See Also :data:`set_relative_phases`
        """
        return self._config.phase_min

    @phasemin.setter
    def phasemin(self, value):
        if isinstance(value, numbers.Number):
            self._config.phase_min = value

    @property
    def phasemax(self):
        """
        The maximum value for the phase range in degrees, where 0 equals current setting

        See Also :data:`set_relative_phases`
        """
        return self._config.phase_max

    @phasemax.setter
    def phasemax(self, value):
        if isinstance(value, numbers.Number):
            self._config.phase_max = value

    @property
    def phaseinterval(self):
        """
        The step of the phase range in degrees

        See Also :data:`set_relative_phases`
        """
        return self._config.phase_interval

    @phaseinterval.setter
    def phaseinterval(self, value):
        if isinstance(value, numbers.Number):
            self._config.phase_interval = value

    @property
    def amplmin(self):
        """The minimum value for the amplitude range, relative to nominal

        1 equals nominal amplitude

        See also :data:`set_relative_amplitudes`
        """
        return self._config.ampl_min

    @amplmin.setter
    def amplmin(self, value):
        if isinstance(value, numbers.Number):
            self._config.ampl_min = value

    @property
    def amplmax(self):
        """The maximum value for the amplitude range, relative to nominal

        1 equals nominal amplitude

        See also :data:`set_relative_amplitudes`
        """
        return self._config.ampl_max

    @amplmax.setter
    def amplmax(self, value):
        if isinstance(value, numbers.Number):
            self._config.ampl_max = value

    @property
    def amplinterval(self):
        """The step length of the amplitude scan range"""
        return self._config.ampl_interval

    @amplinterval.setter
    def amplinterval(self, value):
        if isinstance(value, numbers.Number):
            self._config.ampl_interval = value

    def get_amplitude_range(self):
        """Returns a numpy array containing all steps in amplitude"""
        return numpy.arange(self.amplmin, self.amplmax + 1e-8, self.amplinterval)

    def get_phase_range(self):
        """Returns a numpy array containing all steps in phase"""
        if self.phasemin > self.phasemax:  # User requested to scan around 180 -> -180
            phasemin = self.phasemin + 360
            phasemax = self.phasemax + 360
            r = numpy.arange(phasemin % 360, phasemax % 360 + 1e-8, self.phaseinterval)
            r[r > 180] -= 360
            return r
        return numpy.arange(self.phasemin, self.phasemax + 1e-8, self.phaseinterval)

    @property
    def tdelta(self):
        """Set the time delta between measurements
        After each set point we wait for this given time before aquiring"""
        return self._config.tdelta

    @tdelta.setter
    def tdelta(self, value):
        if isinstance(value, numbers.Number):
            self._config.tdelta = value

    @property
    def simulator(self):
        return self._backend.simulator

    @property
    def new_settings(self):
        """Return the new settings calculated as a dictionary object"""
        return self._backend.analyser.new_settings

    @property
    def include_waveforms(self):
        """
        Enable/disable inclusion of waveforms
        """
        return self._backend.analyser.strategy.include_waveforms

    @include_waveforms.setter
    def include_waveforms(self, value):
        self._backend.analyser.strategy.include_waveforms = value

    def set_sequence(self, sequence, select_all_monitors=True, select_all_cavities=True):
        """
        Set the sequence to be used

        :param sequence: The name of the sequence
        :param select_all_monitors: If True, acquire from all BPM's
        :param select_all_cavities: If True, select all RF elements for scan.
        """
        self.simulator.sequence = sequence
        self._backend.analyser.set_sequence(self.simulator.sequence)

        if select_all_cavities:
            self.all_cavities = self._backend.analyser.strategy.get_variables()
        else:
            self.all_cavities = []
        if select_all_monitors:
            # Warning
            self.all_monitors = self._backend.analyser.strategy.get_monitors(monitor_types=select_all_monitors)
        else:
            self.all_monitors = []

    @property
    def debug_test_mode(self):
        return self._backend.is_test_mode()

    @debug_test_mode.setter
    def debug_test_mode(self, test_mode):
        """
        Enable or disable test mode, which adds a suffix to each channel

        :param test_mode: bool if True, enable test mode, otherwise disable test mode
        :return:
        """
        self._backend.set_test_mode(test_mode)

    def set_accelerator(self, accelerator=None, frontend="ISRC"):
        """
        Set the accelerator definition to be used

        By default, there is a default configured in OpenXAL, so you
        usually do not need to call this.

        :param frontend: Name of frontend sequence
        :param accelerator: Path to main.xal defining the accelerator
        """

        self._backend.simulator.set_accelerator(accelerator)
        frontend_seq = self._backend.get_sequence(frontend)
        self._frontend_bcms = frontend_seq.getAllNodesOfType("BCM")
        self._frontend_bcm_pv = myepics.PV("ISrc-010:PBI-BCM-001:FlatTopCurrentR")

    def get_all_sequences(self, main_sequence=None):
        return self._backend.simulator.get_all_sequences(main_sequence)

    def get_all_nodes_of_type(self, type_):
        return self._backend.simulator.get_all_nodes_of_type(type_)

    def add_monitor(self, monitor, **kwargs):
        """
        Add a monitor to the list of devices to acquire from.
        """
        return self._backend.add_element_to_list(self.all_monitors, monitor, **kwargs)

    def remove_monitor(self, monitor):
        """
        Remove a monitor from the list of devices to acquire from.
        """
        self._backend.remove_element_from_list(self.all_monitors, monitor)

    def add_pv(self, pv):
        """
        Add a generic PV channel to monitor

        :param pv: The channel to add
        :type pv: str or myepics.PV
        """

        # It should be a epics.PV object
        if isinstance(pv, str):
            pv = myepics.PV(pv)

        # do not add if already existing
        for p in self.all_pvs:
            if p.pvname == pv.pvname:
                return pv
        # connect and verify..
        if not pv.connect(timeout=5):
            pv.get()
            if not pv.connected:
                print(pv)
                raise ValueError(f"Could not connect to PV {pv.pvname}")
        self.all_pvs.append(pv)
        return pv

    def remove_pv(self, pv):
        """
        Remove a generic PV from list of general PV's to monitor.

        If PV is not in list, nothing happens (no warnings etc raised).

        This will not remove any handles from monitors even if a PV might match.

        :param pv: The channel to add
        :type pv: str or myepics.PV
        """
        if not isinstance(pv, str):
            pv = pv.pvname
        for p in self.all_pvs:
            if pv == p.pvname:
                self.all_pvs.remove(p)

    def add_cavity(self, cavity):
        """
        Add a cavity to the list of devices which will be scanned.
        """
        return self._backend.add_element_to_list(self.all_cavities, cavity)

    def remove_cavity(self, cavity):
        """
        Remove a cavity from the list of devices which will be scanned.
        """
        self._backend.remove_element_from_list(self.all_cavities, cavity)

    def run_simulation(self, reset=True):
        """
        Run the simulation to get the reference curves for the current setup.

        :param
        """
        if reset:
            self._backend.analyser.reset_simulated()
        for cavity in self.all_cavities:
            self._backend.analyser.add_variable(cavity)
        self._backend.simulator.init_simulation()
        self._backend.simulator.run_simulation(self.all_cavities)

    def load_simulation(self, cavity, filepath, reset=True):
        """
        Load simulated scan data from a file in CSV format
        """
        if reset:
            self._backend.analyser.reset_simulated()
        if isinstance(cavity, str):
            for cav in self._backend.analyser.strategy.get_variables():
                if cavity == str(cav):
                    cavity = cav
                    break
        if isinstance(cavity, str):
            raise ValueError(f"{cavity} not found")
        if cavity not in self.all_cavities:
            raise ValueError(f"{cavity} not added yet")
        self._backend.analyser.add_variable(cavity)
        self._io.load_simulation(self, cavity, filepath)

    def verify_has_beam(self, min_current=10, retries=10):
        """
        Verify that beam is present. For now we look at all BCM's in LEBT lattice

        :param min_current: Minimum current that should be present [mA]
        :param retries: How many retries before declaring no beam presense
        """
        if self.debug_ignore_nobeam:
            return True
        if not self._frontend_bcm_pv.connected:
            print(f"{self._frontend_bcm_pv.pvname} not connected, cannot verify beam")
            return False
        print(f"Checking {self._frontend_bcm_pv.pvname} for current..")
        if self._frontend_bcm_pv.get() < min_current:
            has_beam = False
            for i in range(retries):
                print("Saw no beam, wait&retry...", self._frontend_bcm_pv.get())
                time.sleep(1)
                if self._frontend_bcm_pv.get() > min_current:
                    has_beam = True
                    break
            if not has_beam:
                return False
        return True

    def update_default_settings(self):
        """
        Get default phase/amplitudes for the cavities
        and store those in default_settings dictionary
        """
        # Store the defaults..
        for cavity in self._backend.get_effective_cavities(self.all_monitors):
            self.default_settings[cavity] = self._backend.analyser.strategy.get_settings(cavity)
            self.log("DEFAULTS", cavity, self.default_settings[cavity])
        for cavity in self.all_cavities:
            if cavity not in self.default_settings:
                self.default_settings[cavity] = self._backend.analyser.strategy.get_settings(cavity)
                self.log("DEFAULTS", cavity, self.default_settings[cavity])

    def _verify_range_approved(self):
        if self.set_relative_amplitudes:
            if max(self.get_amplitude_range()) > 1.2:
                raise ValueError("You have set too high max amplitude for relative amplitudes")

    def load_scan(self, filepath):
        """
        Load scan data from a file in the ESS NeXus format
        """
        self._io.load_scan(self, filepath)

    def save_scan(self, filepath, save_machine_state=True, overwrite=False, compression_opts=3):
        """
        Saving the scan data to a file in the ESS NeXus format.
        This is used after a scan is completed, manually by end user.

        :param filepath: Path where file should be saved
        :param save_machine_state: If True, also store machine state in same file
        :param overwrite: If True, overwrite any existing file
        :param compression_opts: Compression level for gzip. 1-9
        :return: True if file is successfully stored, False otherwise
        """
        self._io.save_scan(self, filepath, save_machine_state, overwrite, compression_opts)

    def run_scan(self, status_receiver=print, threaded=False, reset=False, reset_measurement=True):
        """
        Run the scan with current settings for range and devices to scan/acquire.

        In CLI mode, you can typically just leave all arguments with their default values

        :param status_receiver: (ignore for CLI) A function that is called on every scan update
        :param threaded: If true, run scan in a separate thread (necessary for GUI, ignore for CLI)
        :param reset: Restart any ongoing measurements
        :param reset_measurement: Delete any measurements up until this point, before starting a fresh scan

        """

        # Make sure this is in sync for the strategy
        # For some, a pv put is required also to get settings..
        self._backend.analyser.strategy.debug_no_scanning = self.debug_no_scanning

        if not self.scan_ongoing:
            if reset:
                self._backend.analyser.reset_measured()
                self._step_counter = 0
            if not self.default_settings:
                self.update_default_settings()
            self.scan_status_receiver = status_receiver
            self.scan_ongoing = True
            self.scan_halted = False
        elif self.scan_halted:
            if reset:
                if reset_measurement:
                    self._backend.analyser.reset_measured()
                self._step_counter = 0
                self.scan_halted = False
        else:
            self.log("Scan ongoing, not starting another now")
            return

        self._verify_range_approved()

        # TODO: This does not look ideal...
        self._io.init_file(self)

        self.log("Connecting to all channels...")
        self._backend.connect_all_channels(self.all_cavities + self.all_monitors)
        self.log("Connected")

        if threaded:
            t = threading.Thread(target=self._threaded_scan)
            t.start()
        else:
            return self._threaded_scan()

    def _threaded_scan(self):
        """
        TODO: Also store the readback values of the set points (including e.g. Faraday cup, downstream cavities)

        TODO: This is assuming a standard phase/amplitude scan for now

        TODO: Should optionally save data during the scan
        """
        if sys.flags.debug:
            self.scan_status_receiver("DBG, start scan")

        # Initialize in case of jumping to no beam..
        ampl = None
        phase = None

        has_beam = self.verify_has_beam()
        tot_num_points = len(self.all_cavities) * len(self.get_phase_range()) * len(self.get_amplitude_range())
        for cavity in self.all_cavities:
            self.scan_status_receiver(f"Scanning cavity {cavity}")
            phases = self.get_phase_range()
            if self.set_relative_phases:
                phases += self.default_settings[cavity]["Phase"] - cavity.getDfltCavPhase()
            amplitudes = self.get_amplitude_range()
            if self.set_relative_amplitudes:
                amplitudes *= self.default_settings[cavity]["Amplitude"]
            self.log(f"Phases: min = {min(phases)}, max = {max(phases)}, npoints = {len(phases)}")
            self.log(f"Amplitudes: min = {min(amplitudes)}, max = {max(amplitudes)}, npoints = {len(amplitudes)}")
            time.sleep(5)  # Give user 5s to panic if settings are wrong...

            if self.debug_plot_during_scan:
                print("DBG0, setting up scan plot")
                from bokeh import plotting as bokeh_plotting
                from bokeh.io import push_notebook

                # create a new plot with a title and axis labels
                pfig = bokeh_plotting.figure(title="My Line Plot", x_axis_label="x", y_axis_label="y")
                self.plot_lines_data = {}
                for amp in amplitudes:
                    r = pfig.line(phases, numpy.zeros(len(phases)), line_width=2)  # label=f"{amp:.2f}")
                    self.plot_lines_data[amp] = r.data_source.data

                # show the plot in the Jupyter Notebook
                bokeh_plotting.output_notebook()
                bokeh_plotting.show(pfig, notebook_handle=True)

            monitors = []
            for node in self.all_monitors:
                if self.skip_upstream_bpms and str(node.getType()) in ["BPM"] and node.getSDisplay() < cavity.getSDisplay():
                    # Skipping upstream BPM's..
                    continue
                monitors.append(node)
            self._backend.analyser.add_variable(cavity, monitors)

            # Turn off downstream cavities..
            if not self.debug_no_scanning:
                self._backend.switch_other_cavities(False, cavity, self.all_monitors)

            has_beam = self.verify_has_beam()
            for ampl in amplitudes:
                if self.debug_plot_during_scan:
                    self.plot_y_values = numpy.zeros(len(phases))
                    self.plot_lines_data[amp]["y"] = self.plot_y_values
                for num_phase, phase in enumerate(phases):
                    set_point = [ampl, phase]
                    if self.scan_halted:
                        # Find back the combo we were at..
                        if abs(ampl - self.scan_halted_ampl) > 1e-5 or abs(phase - self.scan_halted_phase) > 1e-5:
                            continue
                        self.scan_halted = False
                    self.scan_halted_ampl = ampl
                    self.scan_halted_phase = phase
                    has_beam = self.verify_has_beam()
                    if not has_beam:
                        break
                    self._step_counter += 1
                    self.scan_status_receiver(f"{self._step_counter}/{tot_num_points} {cavity}; {ampl} ampl, {phase} degrees")
                    monitor_data, pv_data = self._get_measured_data(cavity, ampl, phase, monitors)
                    for monitor in monitors:
                        for data_type in monitor_data[monitor]:
                            self._backend.analyser.add_measured_data(
                                cavity, monitor, set_point, data_type, monitor_data[monitor][data_type]
                            )
                    for pv in self.all_pvs:
                        if pv.count == 1 or self.include_waveforms:
                            self._backend.analyser.add_measured_data(cavity, pv.pvname, set_point, "", pv_data[pv.pvname])
                    self._io.save_measurement(self, cavity, set_point)
                    if self.debug_plot_during_scan:
                        self.plot_y_values[num_phase] = monitor_data[monitors[0]]["phaseAvg"]
                        self.plot_lines_data[amp]["y"] = self.plot_y_values
                        push_notebook()
                    if not has_beam:
                        break
                if not has_beam:
                    break

            # Make sure we reset the cavity before moving on..
            if not self.debug_no_scanning:
                if self.immediate_analysis:
                    self.scan_halted = True
                    try:
                        self.analyse(cavity, monitor_list=self.all_monitors)
                        print(f"Found new settings for {cavity.getId()}: {self.new_settings[cavity]}")
                        self._backend.analyser.strategy.apply_settings(cavity, self.new_settings[cavity], should_have_all=True)
                    except ValueError:
                        print(traceback.print_exc())
                        print("Failed to fit", cavity)
                        print("Back to default settings:", self.default_settings[cavity])
                        self._backend.analyser.strategy.apply_settings(cavity, self.default_settings[cavity], should_have_all=True)
                    self.scan_halted = False
                else:
                    print("Back to default settings:", self.default_settings[cavity])
                    self._backend.analyser.strategy.apply_settings(cavity, self.default_settings[cavity], should_have_all=True)

        if has_beam:
            self.scan_ongoing = False
            self._step_counter = 0
            self.scan_status_receiver("Scan finished")
            self._backend.disconnect_all_channels()
        else:
            self.scan_halted = True
            self.log("\n\n-- Scan halted due to no beam present --\n")
            self.log("Continue by calling run_scan() again")
            self.log(f"Next scan point would be ampl = {ampl}, phase = {phase}")
            self.log("Restart by calling run_scan(reset=True)")

    def _get_measured_data(self, cavity, amplitude, phase, monitors):
        """
        Get measured data from all monitors

        By default, returns a list of phases from each of
        the BPMs in the list provided
        """
        if not self.debug_no_scanning:
            self._backend.analyser.strategy.apply_settings(cavity, dict(Amplitude=amplitude, Phase=phase))

        #  wait until new data has arrived
        time.sleep(self._config.tdelta)

        monitor_data = self._backend.analyser.strategy.get_monitor_data(monitors)

        pv_data = {}
        for pv in self.all_pvs:
            pv_data[pv.pvname] = pv.get()
        return monitor_data, pv_data

    def analyse(self, cavity=None, monitor_list=None):
        """
        Plot some data from the scan, calculate corrections

        :param cavity: If set, only analyse this cavity
        :param monitor_list: List of monitors (data) to use (default all available)
        """

        if self.scan_ongoing and not self.scan_halted:
            self.log("Analysis cancelled, scan currently ongoing")
            return

        self._backend.analyser.strategy.dbg_plot = self.debug_plot_analysis
        if monitor_list is not None:
            self._backend.analyser.strategy.analyser.set_monitor_list(monitor_list)
        if cavity is None:
            self._backend.analyser.analyse(self.all_cavities)
        else:
            cavity = self._backend.get_node_if_str(cavity)
            self._backend.analyser.analyse([cavity])

    def update_new_settings(self, clear_old=False):
        """
        Get the new settings from the analysed scans so far

        :param clear_old: If True, remove any old settings
        :return: Dictionary of new settings
        """
        self._config.new_settings = {}
        for acc_node in self._backend.analyser.new_settings:
            self._config.new_settings[acc_node.getId()] = self._backend.analyser.new_settings[acc_node]

    def plot(self, monitor1, monitor2=None, unwrap=False, show_amplitudes=True):
        """
        Plot the data available so far, one plot for each cavity measured

        If simulated data exists, plot curves

        If measured data exists, plot points (different for each amplitude)

        If set points exists, mark new phase
        """
        sim = self._backend.analyser.simulated_data
        meas = self._backend.analyser.measured_data

        if not sim and not meas:
            return

        from matplotlib import pyplot as plt

        if not self.default_settings:
            self.update_default_settings()
        for cavity in self.all_cavities:
            if cavity not in sim and cavity not in meas:
                continue
            if monitor1 not in sim[cavity].monitors:
                continue

            plt.figure()
            ax = plt.gca()
            title = f"{cavity.getId()}: {monitor1.getId()}"
            if monitor2 is not None:
                title = f"{title} - {monitor2.getId()}"
            plt.title(title)

            if cavity in sim:
                # Plot simulate data..
                data_set = sim[cavity]
                if monitor1 in data_set.set_points:
                    x = numpy.array(data_set.set_points[monitor1]) + self.default_settings[cavity]["Phase"]
                    y = numpy.array(data_set.data_set[monitor1]["phases"])

                    # Sort in range [-180:180]
                    x += 180
                    x %= 360
                    x -= 180
                    xinds = x.argsort()
                    sy = y[xinds[::-1]]
                    sx = x[xinds[::-1]]
                    if monitor2:
                        sy -= data_set.data_set[monitor2]["phases"]
                    ax.plot(sx, sy, label="Simulated")
                    if show_amplitudes and monitor2 is None:
                        if "amplitudes" in data_set.data_set[monitor1]:
                            y_amp = numpy.array(data_set.data_set[monitor1]["amplitudes"])
                            sy_amp = y_amp[xinds[::-1]]

                            ax2 = ax.twinx()
                            ax2.plot(sx, sy_amp, "--", label="Simulated")

            if cavity in meas:
                data_set = meas[cavity]
                if monitor1 in data_set.set_points:
                    x = numpy.array(data_set.set_points[monitor1])
                    y = numpy.array(data_set.data_set[monitor1]["phaseAvg"])
                    if unwrap:
                        y = numpy.unwrap(y, period=360)
                    if monitor2:
                        y2 = data_set.data_set[monitor2]["phaseAvg"]
                        if unwrap:
                            y2 = numpy.unwrap(y2, period=360)
                        y -= y2
                    for amp in numpy.unique(x[:, 0]):
                        mask = x[:, 0] == amp
                        x_m = x[mask, 1]
                        y_m = y[mask] - numpy.average(y[mask])
                        ax.plot(x_m, y_m, ".", label=f"{amp:.3f}")

            if cavity in self._config.new_settings:
                new_sp = self._config.new_settings[cavity]
                lbl = f"{new_sp['Phase']} deg, {new_sp['Amplitude']}"
                ax.axvline(new_sp["Phase"], label=lbl)

            ax.legend()

    def apply_settings(self, cavities, settings=None, parameters=None):
        """
        Apply the new settings to the cavities that are listed.
        Optionally provide new settings as well,
        by default the calculated settings are used.

        The settings should be a dictionary with the cavities as keys,
        each cavity setting a list with [phase, amplitude] to be set.

        :param cavities: list of cavities that should be corrected
        :param settings: optionally provide new settings in dictionary format.
        :param parameters: optionally provide a list of the parameters to change (e.g. ['Phase']).
        :return: True if all successful
        """

        successful = True

        if self.debug_no_scanning:
            return successful

        if settings is None:
            settings = self._backend.analyser.new_settings

        for cavity in cavities:
            if cavity in settings:
                setting = settings[cavity].copy()
                if parameters:
                    should_have_all = False
                    keys = list(setting.keys())
                    for key in keys:
                        if key not in parameters:
                            del setting[key]
                else:
                    should_have_all = True
                self.log(f"INFO: {cavity} being corrected, new settings: {setting}")
                this_success = self._backend.analyser.strategy.apply_settings(cavity, setting, should_have_all)
                successful = successful and this_success
            else:
                self.log(f"WARNING: {cavity} does not have a new setting defined")
                successful = False

        return successful
