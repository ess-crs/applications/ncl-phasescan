import numpy
from . import strategies

# TODO: need to find a more consistent name for variable
# TODO: verify this for some scan with two concurrent variables
# TODO: support our ess_nexus file format for storing data


class DataSet:
    def __init__(self, variable, monitors=None):
        self.variables = variable
        self.data_set = {}
        self.set_points = {}
        self.monitors = []  # Used to set PV channels as attribute in saved file
        if monitors:
            self.monitors = monitors.copy()

    def clear(self):
        for mon in self.monitors:
            self.data_set[mon] = {}
            self.set_points[mon] = []

    def add_data_point(self, monitor, settings, data_type, data):
        if not isinstance(monitor, str) and monitor not in self.monitors:  # Direct PV channels are skipped
            self.monitors.append(monitor)

        if monitor not in self.set_points:
            self.set_points[monitor] = []
        if settings not in self.set_points[monitor]:
            self.set_points[monitor].append(settings)

        if data_type:  # Monitors can hold multiple data sets
            if monitor not in self.data_set:
                self.data_set[monitor] = {}
            if data_type not in self.data_set[monitor]:
                self.data_set[monitor][data_type] = []
            self.data_set[monitor][data_type].append(data)
        else:  # Usually direct PV data
            if monitor not in self.data_set:
                self.data_set[monitor] = []
            self.data_set[monitor].append(data)

    def get_data_point(self, monitor, data_type, settings):
        if settings not in self.set_points[monitor]:
            return numpy.nan
        index = self.set_points[monitor].index(settings)
        if data_type:
            return self.data_set[monitor][data_type][index]
        else:
            return self.data_set[monitor][index]

    def get_last_data_point(self, monitor, data_type=None):
        """
        Get the last obtained data point

        :param monitor: The monitor we want to get the data from
        :param data_type: Optionally the data type
        :return: The last data point
        """
        if data_type:
            return self.data_set[monitor][data_type][-1]
        else:
            return self.data_set[monitor][-1]

    def get_axis(self, monitor, axis):
        """
        Returns all the different measurement points for the given axis (set point index)

        :param monitor: The monitor to get data from
        :param axis: The axis to query
        :return: List of all set points for the axis
        """
        points = []
        for sp in self.set_points[monitor]:
            if sp[axis] not in points:
                points.append(sp[axis])
        return points

    def get_data_length(self, monitor, data_type):
        if data_type:
            data = self.data_set[monitor][data_type][0]
        else:
            data = self.data_set[monitor][0]
        try:
            return len(data)
        except TypeError:
            return 1

    def get_data_for(self, monitor, data_type, axes=None):
        """
        Returns a Numpy ND-array for the given axes
        Each element in the axes should be a list of the parameters to get data from

        :param monitor: The monitor to get data from
        :param data_type: The handle for this data
        :param axes: If provided, a list of set points to get ND array from
        :return: A Numpy ND array of the data points
        """
        import numbers

        if axes is None:
            axes = [self.get_axis(monitor, i) for i in range(len(self.set_points[monitor][0]))]
        data_shape = [len(a) for a in axes]
        data_len = self.get_data_length(monitor, data_type)
        if data_len > 1:
            data_shape.append(data_len)
        data = numpy.zeros(data_shape)
        # TODO: Generalise loop below for N elements (only works for N=2 at the moment)
        warnings = []
        for i in range(len(axes[0])):
            for j in range(len(axes[1])):
                point = self.get_data_point(monitor, data_type, [axes[0][i], axes[1][j]])
                if not isinstance(point, numbers.Number):
                    if len(point) == 0:  # Data retrieval failure..
                        warning = f"WARNING: Data retrieval issue with '{data_type}' on {monitor.getId()}"
                        if warning not in warnings:
                            print(warning)
                            warnings.append(warning)
                        continue
                data[i, j] = self.get_data_point(monitor, data_type, [axes[0][i], axes[1][j]])
        return data

    def get_data_types(self, monitor):
        return self.data_set[monitor].keys()


class Analyser:
    def __init__(self, strategy=None, sequence=None):
        self.simulated_data = {}
        self.measured_data = {}
        self._target_settings = {}
        self.new_settings = {}
        if strategy is None:
            self.__strategy_class = strategies.DefaultStrategy
        else:
            self.__strategy_class = strategy
        self.variables = []
        if sequence is not None:
            self.strategy = strategy(sequence)

    def set_sequence(self, sequence):
        self.strategy = self.__strategy_class(sequence)

    def reset_measured(self):
        for key in self.measured_data:
            self.measured_data[key].clear()

    def reset_simulated(self):
        for key in self.simulated_data:
            self.simulated_data[key].clear()

    def reset(self):
        self.reset_measured()
        self.reset_simulated()
        self._target_settings = {}

    def add_simulated_set(self, variable, monitors, overwrite):
        Analyser.add_dset(self.simulated_data, variable, monitors, overwrite)

    def add_measured_set(self, variable, monitors, overwrite):
        Analyser.add_dset(self.measured_data, variable, monitors, overwrite)

    @staticmethod
    def add_dset(dictionary, variable, monitors, overwrite):
        if overwrite or (variable not in dictionary):
            dictionary[variable] = DataSet(variable, monitors)

    def add_variable(self, variable, monitors=None, overwrite=False):
        self.add_simulated_set(variable, monitors, overwrite)
        self.add_measured_set(variable, monitors, overwrite)
        self._target_settings[variable] = self.strategy.get_settings(variable, nominal=True)
        if variable not in self.variables:
            self.variables.append(variable)

    def set_target_setting(self, variable, parameter, value):
        self._target_settings[variable][parameter] = value

    def add_measured_data(self, variable, monitor, settings, data_type, data):
        if variable not in self.measured_data:
            raise ValueError(f"{variable} not in data set, did you load/run simulation yet?")
        self.measured_data[variable].add_data_point(monitor, settings, data_type, data)

    def add_simulated_data(self, variable, monitor, settings, data_type, data):
        if data_type not in ["phases", "amplitudes"]:
            raise ValueError(f"Wrong data type {data_type} for simulated data")
        self.simulated_data[variable].add_data_point(monitor, settings, data_type, data)

    def load_simulation(self, file_path):
        print("Loading simulation not yet defined")
        pass

    def has_measured_data(self, cavity):
        return len(self.measured_data[cavity].data_set.keys()) > 0

    def analyse(self, cavities):
        # Use the analyse function of the chosen strategy to
        # get new settings
        self.new_settings = {}

        for cavity in cavities:
            target = self._target_settings[cavity]
            if self.has_measured_data(cavity):
                self.new_settings[cavity] = self.strategy.analyse(cavity, target, self.measured_data[cavity], self.simulated_data[cavity])
            else:
                print(f"No measured data for {cavity}, skipping")
