from .analyser import Analyser
from .simulator import Simulator
from . import myepics
import sys
import asyncio


def asynchronous(f):
    def wrapped(*args, **kwargs):
        return asyncio.get_event_loop().run_in_executor(None, f, *args, **kwargs)


class Phaser:
    def __init__(self, oxal, config, strategy=None):
        self.analyser = Analyser(strategy)
        self.simulator = Simulator(oxal)
        self.simulator.analyser = self.analyser
        self._config = config
        self.log = print

    def is_test_mode(self):
        return self.simulator.test_mode

    def set_test_mode(self, test_mode):
        self.simulator.test_mode = test_mode
        return self.is_test_mode()

    def get_sequence(self, sequence):
        return self.simulator._model.accelerator.getSequence(sequence)

    def get_node_if_str(self, element):
        if isinstance(element, str):
            return self.simulator.get_node(element)
        return element

    def add_element_to_list(self, list_, element, warn=True):
        element = self.get_node_if_str(element)
        if element is None:
            if warn:
                print(f"WARNING, could not add {element}")
            return
        if element not in list_:
            if sys.flags.debug:
                self.log(f"DBG, adding {element}")
            list_.append(element)
        return element

    def remove_element_from_list(self, list_, element):
        if not isinstance(element, str):
            element = str(element)
        if sys.flags.debug:
            self.log(f"DBG, rm {element}")
        for i in range(len(list_)):
            if str(list_[i]) == element:
                del list_[i]
                return element

    def get_effective_cavities(self, elements):
        """
        Get a list of all cavities that are positioned within
        the list of elements.
        TODO: Check behaviour for PM's in DTL tank

        :param elements: The list of elements (nodes)
        :return: List of cavities with a position within these nodes positions
        """
        min_s = 1e6
        max_s = 0.0
        for monitor in elements:
            s = monitor.getSDisplay()
            if s > max_s:
                max_s = s
            if s < min_s:
                min_s = s
        effective_cavities = []
        for cav in self.analyser.strategy.get_variables():
            s = cav.getSDisplay()
            if min_s < s < max_s:
                effective_cavities.append(cav)
        return effective_cavities

    # @asynchronous
    def set_cavity_amplitude_async(self, node, amplitude):
        self.log(f"-- Set new cavity amplitude for {node.getId()} to {amplitude}")
        self.analyser.strategy.apply_settings(node, dict(Amplitude=amplitude))

    def switch_other_cavities(self, turn_on, cavity, all_monitors, settings=None):
        """
        Switch on/off cavities that are not the one being modulated

        TODO: Cavity blanking necessary? Would delay be faster?
        """
        # loop = asyncio.get_event_loop()
        groups = []
        if self._config.upstream_off or self._config.downstream_off:
            for node in self.get_effective_cavities(all_monitors):
                if node != cavity:
                    if turn_on:
                        new_amplitude = settings[node]["Amplitude"]
                    else:
                        new_amplitude = 0.0
                    if self._config.downstream_off and node.getSDisplay() > cavity.getSDisplay():
                        print("DBG", groups, self, self.set_cavity_amplitude_async)
                        # groups.append(self.set_cavity_amplitude_async(node, new_amplitude))
                        self.set_cavity_amplitude_async(node, new_amplitude)
                    if self._config.upstream_off and node.getSDisplay() < cavity.getSDisplay():
                        # groups.append(self.set_cavity_amplitude_async(node, new_amplitude))
                        self.set_cavity_amplitude_async(node, new_amplitude)
        # loop.run_until_complete(asyncio.gather(*groups))
        self.log("Cavities at new amplitudes")

    def connect_all_channels(self, nodes_list):
        """
        Initial connection using p4p is slow,
        this can be much faster by connecting to a channel list initially
        """
        clist = []

        self._monitored_channels = []
        for m in nodes_list:
            used_handles = self.analyser.strategy.get_handles(m)
            if used_handles:
                for handle in used_handles:
                    ch_name = str(m.getChannel(handle).getId())
                    if ch_name not in clist:
                        clist.append(ch_name)
                        pv = myepics.PV(ch_name)
                        pv.monitor(True)
                        self._monitored_channels.append(pv)

    def disconnect_all_channels(self):
        if hasattr(self, "_monitored_channels"):
            for pv in self._monitored_channels:
                pv.disconnect()
