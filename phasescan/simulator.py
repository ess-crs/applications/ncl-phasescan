from . import model
from . import strategies
import numpy as np
import sys


class Simulator:
    def __init__(self, oxal, strategy=strategies.DefaultStrategy):
        self._model = model.Model(oxal)
        self._StrategyClass = strategy
        self._strategy = None
        self.probe = None
        self.model = None
        self._analyser = None
        self._curves = {}
        self._range = np.arange(-180, 181, 2)

    @property
    def test_mode(self):
        return self._model.test_mode

    @test_mode.setter
    def test_mode(self, mode):
        self._model.test_mode = mode

    @property
    def accelerator_path(self):
        return self._model.get_accelerator_path()

    def set_accelerator(self, accelerator=None):
        self._model.accelerator = accelerator

    def init_simulation(self):
        self.probe = self._model.get_probe()
        self.model = self._model.get_model(self.probe)

    def get_phase_at_monitor(self, monitor, trajectory):
        probe_state = trajectory.statesForElement(monitor.getId())[0]
        return probe_state.getTime() * 360.0 * monitor.getBPMBucket().getFrequency()

    def run_simulation(self, variables):
        num_points = len(self._range)

        for variable in variables:
            dflt_phase = variable.getDfltCavPhase()
            self._curves[variable] = {}
            ref_monitor_phases = {}
            for monitor in self._strategy.get_monitors(variable):
                self._curves[variable][monitor] = np.zeros(num_points)
            for i in range(num_points):
                phase = self._range[i]
                if sys.flags.debug:
                    print(f"DBG sim phase {phase} {dflt_phase} {phase - dflt_phase}")
                # Set variable to phase, note that phase = 0 equals the set point of given variable (cavity)
                variable.setDfltCavPhase(phase + dflt_phase)
                trajectory = self._model.get_trajectory_from_model(self.model)
                for monitor in self._strategy.get_monitors(variable):
                    if monitor.getType() == "BPM":
                        monitor_phase = self.get_phase_at_monitor(monitor, trajectory)
                        if i == 0:
                            ref_monitor_phases[monitor] = monitor_phase
                        self._analyser.add_simulated_data(variable, monitor, phase, "phases", monitor_phase - ref_monitor_phases[monitor])
            variable.setDfltCavPhase(dflt_phase)

    @property
    def analyser(self):
        return self._analyser

    @analyser.setter
    def analyser(self, analyser):
        self._analyser = analyser

    @property
    def sequence(self):
        return self._model.sequence

    @sequence.setter
    def sequence(self, sequence):
        self._model.sequence = sequence
        self._strategy = self._StrategyClass(self._model.sequence)

    def get_all_sequences(self, main_sequence=None):
        if main_sequence is None:
            self.set_accelerator()
            main_sequence = self._model.accelerator
        return [seq for seq in main_sequence.getSequences()]

    def get_all_nodes_of_type(self, node_type):
        if self._model and self._model.sequence:
            return self._model.sequence.getAllNodesOfType(node_type)

    def get_node(self, node_str):
        if self._model and self._model.sequence:
            return self._model.sequence.getNodeWithId(node_str)
