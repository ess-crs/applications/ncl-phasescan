from abc import ABCMeta, abstractmethod


class AbstractStrategy(metaclass=ABCMeta):
    """
    This should be replaceable by other strategies

    Each strategy needs a defined list of elements to scan, and monitors required for analysis

    Then, each strategy needs the analysis function defined
    """

    def get_variables(self):
        """
        The Default Strategy returns a list of RF Cavities in the section.

        :return: The list of variables that should be scanned, and a list of the parameters to scan for this node
        """
        nodes = []
        for variable in self.variables:
            for node in self.sequence.getAllNodesOfType(variable):
                nodes.append(node)
        return nodes

    def get_monitors(self, variable=None, monitor_types=None):
        """
        For each variable, the class should tell what monitors are needed while scanning this variable.
        If variable is set to None, return list of all monitors used by any variable.

        :param variable: Provide monitors used for this variable
        :param monitor_types: Provide a list of types to include, otherwise provide all types for given strategy
        :return: List of monitors
        """
        monitors = []
        if not isinstance(monitor_types, list):
            monitor_types = self._monitor_node_types
        for node_type in monitor_types:
            for node in self.sequence.getAllNodesOfType(node_type):
                if (variable is None) or str(node.getType()) not in ["BPM"] or (node.getSDisplay() > variable.getSDisplay()):
                    monitors.append(node)
        return monitors

    def get_handles(self, monitor):
        """
        Returns the active handles for the given monitor
        """
        m_type = monitor.getType()
        if m_type in self._monitor_node_handles:
            return self._monitor_node_handles[monitor.getType()]
        else:
            print(f"WARNING: found no handles for {m_type}")

    def get_monitor_data(self, monitors):
        """
        For each monitor, and each handle, get the data and return

        :param monitors: List of monitors to retrieve data from
        :return: A dictionary of the measured data
        """
        from . import myepics

        data = {}
        for m in monitors:
            data[m] = {}
            t = m.getType()
            for h in self._monitor_node_handles[t]:
                chan = m.findChannel(h)
                if chan is not None:
                    data[m][h] = myepics.get((chan.getId()))
                else:
                    print(f"Could not connect to {h} of {m}")
                    data[m][h] = None

        return data

    def get_all_parameters(self):
        all_parameters = []
        for key in self.parameters:
            for parameter in self.parameters[key]:
                if parameter not in all_parameters:
                    all_parameters.append(parameter)
        return all_parameters

    def _check_valid_parameters(self, parameters, should_have_all=False):
        if not isinstance(parameters, (list, dict, tuple)):
            parameters = [parameters]
        all_parameters = self.get_all_parameters()
        for parameter in parameters:
            if parameter not in all_parameters:
                raise ValueError(f"Unknown parameter {parameter} for current strategy")
        if should_have_all:
            for parameter in all_parameters:
                if parameter not in parameters:
                    raise ValueError(f"Provided settings were missing {parameter}")

    @abstractmethod
    def get_setting(self, variable, parameter, nominal):
        """

        :param variable: The variable to get the parameter from
        :param parameter: The parameter you want the setting of
        :param nominal: If true, return the nominal (design) setting
        :return: The setting of parameter for variable in question
        """

    def get_settings(self, variable, nominal=False):
        settings = {}
        for parameter in self.get_all_parameters():
            settings[parameter] = self.get_setting(variable, parameter, nominal)
        return settings

    @abstractmethod
    def apply_settings(self, variable, settings, should_have_all):
        """

        :param variable: variable to modify
        :param settings: dictionary of new settings to apply
        :param should_have_all: if True, new settings should contain all parameters (raise error otherwise)
        :return: None
        """

    @abstractmethod
    def analyse(self, variable, target, measured_data, simulated_data, axes):
        """
        Analysis of measured/simulated data for a given variable (or set of variables)

        :param variable: The variable to be corrected
        :param target: The target parameters for the variable (nominal by default)
        :param measured_data: The DataSet object for the measured data of variable
        :param simulated_data: The DataSet object for the simulated data of variable
        :return: The calculated set point for the given variable
        """
        pass

    @property
    @abstractmethod
    def sequence(self):
        # TODO this does not fit here, move it elsewhere I think
        pass

    @sequence.setter
    @abstractmethod
    def sequence(self, seq):
        pass

    @property
    @abstractmethod
    def parameters(self):
        pass

    @property
    def _monitor_node_types(self):
        """
        A list of observable nodes this strategy makes use of.
        """
        return self._monitor_node_handles.keys()

    @property
    def _monitor_node_handles(self):
        """
        A list of handles for each observable nodes
        this strategy should store
        """
        handle_dict = {"BPM": ["phaseAvg", "amplitudeAvg", "xAvg", "yAvg"], "FC": ["currentAvg"], "BCM": ["currentAvg"]}

        if self.include_waveforms:
            handle_dict["BPM"].append("phaseArray")
            handle_dict["FC"].append("currentArray")
            handle_dict["BCM"].append("currentArray")

        return handle_dict

    @property
    def variables(self):
        """
        A list of nodes that this strategy modifies
        """
        return ["RF"]
