class ScanConfig:
    """
    Config class holding configurations for scans

    This should always be possible to dump with yaml (or similar)
    for save/load of config.

    TODO: More settings moved here
    """

    def __init__(self):
        self.phase_min = -30
        self.phase_max = 30
        self.phase_interval = 15
        self.ampl_min = 1.0
        self.ampl_max = 1.0
        self.ampl_interval = 0.05
        self.tdelta = 0.3
        self.downstream_off = False
        self.upstream_off = False
        self.new_settings = {}
