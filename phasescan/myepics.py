from p4p.client.thread import Context
import epics

ctxt = Context("pva")
# TODO: getting authentication warnings

monitored_channels = {}

use_pva = True


def _clean_pv_name(pv_name):
    if not isinstance(pv_name, (str, list)):
        pv_name = str(pv_name)
    return pv_name


def get(pv_name, **kwargs):
    """
    If we are already monitoring this PV,
    you will always get the monitored value.
    This may be causing logical issues for certain things.
    """
    if use_pva:
        if isinstance(pv_name, str):
            if pv_name in monitored_channels:
                return monitored_channels[pv_name].get(**kwargs)
        return ctxt.get(_clean_pv_name(pv_name), **kwargs)
    else:
        if isinstance(pv_name, str):
            if pv_name in monitored_channels:
                return monitored_channels[pv_name].get(**kwargs)
        return epics.caget(_clean_pv_name(pv_name), **kwargs)


def put(pv_name, value, **kwargs):
    if use_pva:
        return ctxt.put(_clean_pv_name(pv_name), value, **kwargs)
    else:
        return epics.put(_clean_pv_name(pv_name), value, **kwargs)


class PV:
    def __init__(self, pv_name):
        self._pv_name = _clean_pv_name(pv_name)
        self._last_value = None
        self._monitor_subscription = None

    def get(self, **kwargs):
        if self._last_value is None:
            if use_pva:
                return ctxt.get(self._pv_name, **kwargs)
            else:
                return epics.caget(self._pva_name, **kwargs)
        else:
            return self._last_value

    def put(self, value):
        return put(self._pv_name, value)

    def connect(self, timeout=5.0):
        self.get(timeout=timeout)
        pass

    def connected(self):
        res = self.get(throw=False)
        if isinstance(res, TimeoutError):
            return False
        return True

    def monitor(self, monitor):
        """
        Turn on monitoring for this PV
        """
        if monitor is True and self._monitor_subscription is None:
            self._monitor_subscription = ctxt.monitor(self._pv_name, cb=self._monitor_callback)
            monitored_channels[self._pv_name] = self
        elif monitor is False and self._monitor_subscription is not None:
            self._monitor_subscription.close()
            self._monitor_subsciption = None
            self._last_value = None
            monitored_channels.pop(self._pv_name)

    def _monitor_callback(self, pv):
        self._last_value = pv

    def disconnect(self):
        self.monitor(False)

    def __del__(self):
        self.disconnect()

    @property
    def pvname(self):
        return self._pv_name

    @property
    def count(self):
        # Check length of pv data:
        pv_data = get(self._pv_name)
        if hasattr(pv_data, "__iter__"):
            return len(pv_data)
        return 1
