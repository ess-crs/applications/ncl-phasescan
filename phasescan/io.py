import os
import sys
import time
import datetime


def get_run_string():
    return datetime.datetime.today().strftime("%Y%m%d") + "A"


class IO:
    def __init__(self, backend):
        self._nexus_file = None
        self._config_file = None
        self._backend = backend
        self.log = print

    def get_filepath(self):
        if self._nexus_file:
            return self._nexus_file.get_filename()

    def set_filepath(self, filepath, overwrite):
        if filepath is not None:
            if self._nexus_file is None:
                import essnexus

                if os.path.exists(filepath) and not overwrite:
                    raise ValueError(f"{filepath} exists and cannot be extended, overwrite or change filename")
                self._nexus_file = essnexus.File(filepath, overwrite)
            else:
                if self.get_filepath() != filepath:
                    print(f"WARNING: nexus file already set to {self.get_filepath()}, not changing")

    def close_file(self):
        if self._nexus_file is not None:
            self._nexus_file.close()

    def save_config(self, filename, config):
        """
        Save a config file with scan settings

        TODO: Should use safe loader
        :param filename: name of config file to load
        """
        import yaml

        self._config_file = filename

        with open(filename, "w") as f:
            yaml.dump(config, f)

    def load_config(self, filename):
        """
        Load a config file with scan settings

        TODO: Should use safe loader
        :param filename: name of config file to load
        """
        import yaml
        from yaml.loader import UnsafeLoader

        self._config_file = filename

        with open(filename, "r") as f:
            return yaml.load(f, Loader=UnsafeLoader)

    def load_simulation(self, scanner, cavity, filepath):
        """
        Load a simulation from file

        TODO: Not yet implemented
        """
        import csv

        print("")
        data = {}
        analyser = scanner._backend.simulator._analyser
        all_bpms = {str(m): m for m in scanner.all_monitors}
        with open(filepath) as csvfile:
            reader = csv.DictReader(csvfile)
            for k in reader.fieldnames:
                if k.lower() == "phase":
                    data[k] = []
                else:
                    bpm = ":".join(k.split(":")[:-1])
                    if bpm in all_bpms:
                        data[bpm] = {"Phase": [], "Amplitude": []}
                    else:
                        self.log(f"WARNING: {bpm} found in simulation file, but is not used at the moment. Ignored")
            for row in reader:
                for bpm in data:
                    if bpm == "Phase":
                        data[bpm].append(float(row[bpm]))
                    else:
                        data[bpm]["Phase"].append(float(row[bpm + ":Phase"]))
                        if bpm + ":Amplitude" in row:
                            data[bpm]["Amplitude"].append(float(row[bpm + ":Amplitude"]))
                        else:
                            data[bpm]["Amplitude"].append(1.0)

            for i, phase in enumerate(data["Phase"]):
                for bpm in data:
                    if bpm != "Phase":
                        analyser.add_simulated_data(cavity, all_bpms[bpm], phase, "phases", data[bpm]["Phase"][i])
                        analyser.add_simulated_data(cavity, all_bpms[bpm], phase, "amplitudes", data[bpm]["Amplitude"][i])
        self.log(f"Loaded sim for {cavity} from {filepath}")

    def init_file(self, scanner):
        """
        Run this before a scan starts, after it has been configured.
        """
        if self._nexus_file is None:
            return

        sequence = self._backend.analyser.strategy.sequence

        self._nexus_file.create_file_entry(
            run_cycle=get_run_string(),
            title="RF Scan data",
            description=f"Scan of {len(scanner.all_cavities)}  cavities in {sequence}",
        )
        for cavity in scanner.all_cavities:
            if not self._nexus_file.has_instrument(str(cavity)):
                self._nexus_file.add_instrument(str(cavity), nxtype="NXradio_frequency", location=str(sequence))
        for bpm in scanner.all_monitors:
            if not self._nexus_file.has_instrument(str(bpm)):
                self._nexus_file.add_beam_instrumentation(str(bpm), location=str(sequence), epics_channels="0.0")

        if not self._nexus_file.has_dataset("data"):
            self._nexus_file.add_dataset("data", "amplitudes", scanner.get_amplitude_range())  # , attributes=dict(units="Relative to max"))
            self._nexus_file.add_dataset("data", "phases", scanner.get_phase_range())  # , attributes=dict(units="degree"))

    def save_measurement(self, scanner, cavity, set_point, compression_opts=3):
        """
        Save one measurement point

        Essentially copy from the old save_scan(), but dimension of the saved data has changed.
        We now store the data set in a stream.

        :param cavity: the cavity currently being scanned
        :param set_point: list with set point [amp, phase]
        :param compression_opts: Compression level for gzip. 1-9
        """
        if self._nexus_file is None:
            return
        measurements = self._backend.analyser.measured_data
        self._nexus_file.add_dataset(str(cavity), "set_points", set_point, resizable=True)
        for monitor in measurements[cavity].monitors:
            for key in measurements[cavity].get_data_types(monitor):
                attrs = {"channel": str(monitor.findChannel(key).getId())}
                data_point = measurements[cavity].get_last_data_point(monitor, data_type=key)
                self._nexus_file.add_dataset(
                    str(cavity), f"{str(monitor)}/{key}", data_point, attrs=attrs, compression_opts=compression_opts, resizable=True
                )
        for pv in scanner.all_pvs:
            if pv.count == 1 or scanner.include_waveforms:
                data_point = measurements[cavity].get_last_data_point(pv.pvname, data_type="")
                self._nexus_file.add_dataset(str(cavity), f"{pv.pvname}", data_point, compression_opts=compression_opts, resizable=True)

    def save_scan(self, scanner, filepath, save_machine_state=True, overwrite=False, compression_opts=3):
        """
        Saving the scan data to a file in the ESS NeXus format.

        TODO: Check if file path has right file ending
        TODO: epics channel from bpm
        TODO: fix for code restructure
        TODO: Deprecated?

        :param filepath: Path where file should be saved
        :param save_machine_state: If True, also store machine state in same file
        :param overwrite: If True, overwrite any existing file
        :param compression_opts: Compression level for gzip. 1-9
        :return: True if file is successfully stored, False otherwise
        """
        import essnexus

        sequence = self._backend.analyser.strategy.sequence
        measurements = self._backend.analyser.measured_data

        self.log(f"Storing simulation in {filepath}")
        if os.path.exists(filepath):
            if overwrite:
                print(f"WARNING: {filepath} already exists, deleting...")
                time.sleep(5)  # give user 5s to panic.. :)
                os.remove(filepath)
            else:
                print(f"WARNING: {filepath} already exists, will not overwrite")
                return False

        f = essnexus.File(filepath)
        if save_machine_state:
            f.create_machine_state(lattice_path=self._backend.simulator.accelerator_path)

        f.create_file_entry(
            run_cycle=get_run_string(),
            title="RF Scan data",
            description=f"Scan of {len(scanner.all_cavities)}  cavities in {sequence}",
        )
        for cavity in scanner.all_cavities:
            f.add_instrument(str(cavity), nxtype="NXradio_frequency", location=str(sequence))
        for bpm in scanner.all_monitors:
            f.add_beam_instrumentation(str(bpm), location=str(sequence), epics_channels="0.0")

        f.add_dataset("data", "amplitudes", scanner.get_amplitude_range())  # , attributes=dict(units="Relative to max"))
        f.add_dataset("data", "phases", scanner.get_phase_range())  # , attributes=dict(units="degree"))
        for cavity in measurements:
            for monitor in measurements[cavity].set_points:  # save set points once (this is same for all.. ideally)
                f.add_dataset(str(cavity), "set_points", measurements[cavity].set_points[monitor])
                break
            for monitor in measurements[cavity].monitors:
                for key in measurements[cavity].get_data_types(monitor):
                    attrs = {"channel": str(monitor.findChannel(key).getId())}
                    data_sets = measurements[cavity].get_data_for(monitor, data_type=key)
                    f.add_dataset(str(cavity), f"{str(monitor)}/{key}", data_sets, attrs=attrs, compression_opts=compression_opts)
            for pv in scanner.all_pvs:
                if pv.count == 1 or scanner.include_waveforms:
                    data_sets = measurements[cavity].get_data_for(pv.pvname, data_type="")
                    f.add_dataset(str(cavity), f"{pv.pvname}", data_sets, compression_opts=compression_opts)
        self.log("File saved")

    def load_scan(self, scanner, filepath):
        """
        Load scan data from a file in the ESS NeXus format

        TODO: Use nexus instead of h5py?
        """
        import h5py

        with h5py.File(filepath, "r") as hf:
            entry = hf["entry"]
            for key in entry:
                if key not in ["data", "instruments", "machine_state", "user"]:
                    cavity = scanner.add_cavity(key)
                    if cavity is not None:
                        self.log(f"Found scan data for cavity {key}")
                        sp = entry[key]["set_points"]
                        for mkey in entry[key]:
                            if mkey not in ["set_points"]:
                                monitor = scanner.add_monitor(mkey, warn=False)
                                if monitor is None:
                                    # generic PV channel assumed
                                    pv = scanner.add_pv(mkey)
                                    if sys.flags.debug:
                                        self.log(f"Found PV data for cavity {key}, pv {mkey}")
                                    for i, p in enumerate(entry[key][mkey]):
                                        self._backend.analyser.add_measured_data(cavity, pv.pvname, sp[i].tolist(), "", p)
                                else:
                                    if sys.flags.debug:
                                        self.log(f"Found measured data for cavity {key}, monitor {mkey}")
                                    for dtype in entry[key][mkey]:
                                        if sys.flags.debug:
                                            self.log(f"Adding {mkey} - {dtype}")
                                        for i, p in enumerate(entry[key][mkey][dtype]):
                                            self._backend.analyser.add_measured_data(cavity, monitor, sp[i].tolist(), dtype, p)
            self.log(f"Scan data from {filepath} loaded")
