import numpy
from . import _shared_fitting


class Analyser:
    def __init__(self, fixed_phase, dbg_plot):
        """
        This fitting requires a full simulated curve for fitting.

        For each amplitude, we look at each downstream BPM and
        do a curve fit xi2 style. This gives the fitted phase from each
        individual curve. We then do a weighted average of all phases.

        For amplitude we look again at each individual BPM, and for
        every amplitude measured we get a max excursion on the fitted curve.
        These excursions provide a linear curve and we compare to the
        nominal expected excursion from the simulated curve. That means
        one amplitude estimate per BPM, and we do again a weighted average
        of these amplitudes.

        :param fixed_phase: Do not fit phase
        :param dbg_plot: Plot (debugging mode)
        """
        self.fixed_phase = fixed_phase
        if self.fixed_phase:
            print("WARNING: fixed phase ignored for current method")
        self.dbg_plot = dbg_plot
        self.monitor_list = None
        self.error_threshold = 4.0
        self.unwrap_with_array = False  #: if True, use phaseArray to unwrap phases if available.
        self.sine_fit = False  #: if True, fit to sine curve, otherwise fit to simulated curve.

    def set_monitor_list(self, mon_list):
        self.monitor_list = mon_list

    def get_phases_from_array(self, phases, phase_array):
        # hard coded ROI for now..
        roi = [3768, 3790]
        new_phases = phases.copy()
        for i in range(len(phases)):
            array = phase_array[i, roi[0] : roi[1]]
            diff = max(array) - min(array)
            if diff > 15 and max(array) > 160:
                mask = array > 160
                array[mask] -= 360
                array += 360
                new_phases[i] = numpy.average(array)
        return new_phases

    def get_mask_bad_points(self, data, limit=0.3):
        """
        Assumes that there should be a max
        limit for change between points.

        Limit is relative to max(data) - min(data), so the default 0.2 means 10% change per point max

        Returns a mask that can be used to take out the bad points
        """
        limit = limit * (max(data) - min(data))
        mask = numpy.array([True] * len(data))
        for i in range(1, len(data) - 1):
            if abs(data[i - 1] - data[i]) > limit and abs(data[i] - data[i + 1]) > limit:
                mask[i] = False
        return mask

    def analyse(self, variable, target, measured_data, simulated_data):
        """

        TODO: weight should be correlated with the BPM amplitude as well (to ignore debunched beam reading)
        """

        if self.monitor_list is None:
            self.monitor_list = measured_data.monitors
        y = []
        for monitor in measured_data.monitors:
            for ampl in measured_data.get_axis(monitor, 0):
                if ampl not in y:
                    y.append(ampl)
        y.sort()
        y = numpy.array(y)
        if self.dbg_plot:
            from matplotlib import pyplot as plt

            plt.figure(figsize=(16, 12))

        # Count number of monitors:
        nmonitors = 0
        for monitor in self.monitor_list:
            if monitor in measured_data.data_set and monitor in simulated_data.data_set:
                nmonitors += 1
        all_estimates = numpy.zeros((nmonitors, 4))
        i_mon = -1
        for monitor in self.monitor_list:
            if monitor not in measured_data.data_set:
                continue
            if monitor not in simulated_data.data_set:
                # This method requires simulated data for fitting
                continue
            i_mon += 1
            amplitudes = measured_data.get_axis(monitor, 0)
            phases = numpy.array(measured_data.get_axis(monitor, 1))
            sim_amplitude = max(simulated_data.data_set[monitor]["phases"]) - min(simulated_data.data_set[monitor]["phases"])
            sim_avg = numpy.average(simulated_data.data_set[monitor]["phases"])
            sim_amplitude /= 2
            sim_x = numpy.array(simulated_data.set_points[monitor])
            sim_y = (numpy.array(simulated_data.data_set[monitor]["phases"]) - sim_avg) / sim_amplitude

            estimates = numpy.zeros((len(amplitudes), 3))
            for i_amp, amplitude in enumerate(amplitudes):
                axes = [[amplitude], phases]
                data = measured_data.get_data_for(monitor, data_type="phaseAvg", axes=axes)[0]
                if self.unwrap_with_array and "phaseArray" in measured_data.get_data_types(monitor):
                    data_arrays = measured_data.get_data_for(monitor, data_type="phaseArray", axes=axes)[0]
                    data = self.get_phases_from_array(data, data_arrays)
                mask = numpy.isnan(data) == False  # Skip any missing points..
                mask = mask & self.get_mask_bad_points(data)
                data = data[mask]
                phases_m = phases[mask]

                if self.sine_fit:
                    fit_function, est_ampl, est_phase, est_mean, error = _shared_fitting.sine_fit(data, phases_m)
                else:
                    fit_function, est_ampl, est_phase, est_mean, error = _shared_fitting.curve_fit(data, phases_m, sim_x, sim_y)
                estimates[i_amp] = [est_phase, sim_amplitude / est_ampl * amplitude, error]
                if self.dbg_plot:
                    meas_x = _shared_fitting.unwrap(data) - est_mean
                    meas_x /= sim_amplitude
                    x_fit = numpy.arange(-180, 180)
                    meas_x_fit = fit_function(x_fit) - est_mean
                    meas_x_fit /= sim_amplitude
                    plt.plot(x_fit, meas_x_fit, "--", label=f"{monitor.getId()}: {amplitude * 1e3:.2f} kV fit")
                    plt.plot(phases_m, meas_x, ".-", label=f"{monitor.getId()}: {amplitude * 1e3:.1f} kV")
                    plt.legend()

            mask = estimates[:, 2] < self.error_threshold
            if len(estimates[mask]):
                print("E", estimates)
                new_phase_est, new_phase_std = _shared_fitting.weighted_avg_and_std(estimates[mask, 0], 1 / estimates[mask, 2], unwrap=360)
                new_amp_est, new_amp_std = _shared_fitting.weighted_avg_and_std(estimates[mask, 1], 1 / estimates[mask, 2])
                all_estimates[i_mon] = [new_phase_est, new_phase_std, new_amp_est, new_amp_std]
                if self.dbg_plot:
                    x = sim_x + new_phase_est
                    # Plot in [-180:180] window:
                    mask = x < -180
                    x[mask] += 360
                    mask = x > 180
                    x[mask] -= 360
                    xinds = x.argsort()
                    sorted_x = x[xinds[::-1]]
                    sorted_y = sim_y[xinds[::-1]]

                    plt.plot(sorted_x, sorted_y, label=f"Simulated {monitor.getId()}")

        if numpy.sum(all_estimates) == 0:
            print(all_estimates)
            raise ValueError(f"Could not find any good fit for amplitude&phase, fit threshold is {self.error_threshold}")
        mask = abs(all_estimates[:, 0]) > 0
        new_phase_est = numpy.mean(all_estimates[mask, 0])
        new_phase_std = numpy.sqrt(numpy.sum(all_estimates[mask, 1] ** 2))
        new_amp_est = numpy.mean(all_estimates[mask, 2])
        new_amp_std = numpy.sqrt(numpy.sum(all_estimates[mask, 3] ** 2))
        print(f"Estimated phase {new_phase_est} +- {new_phase_std}")
        print(f"Estimated amplitude {new_amp_est} +- {new_amp_std}")
        if self.dbg_plot:
            plt.axvline(new_phase_est, ls="--", label=f"{new_amp_est:.3f} : {new_phase_est:.3f}")
            plt.legend()
            plt.show()
        return dict(Phase=new_phase_est, Amplitude=new_amp_est)
