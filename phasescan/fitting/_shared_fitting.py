import numpy


def func_fit(x, y, func):
    """
    Fit a function that takes x values and an array of 3 arguments

    The three arguments expected to be
     - an amplitude scaling of the function
     - a shift of the function relative to the min (phase)
     - a mean value of the function


    Initial guess based on a sine function fit
    """
    from scipy import optimize

    y = unwrap(y)

    def optimize_func(j):
        return func(x, j) - y

    guess_mean = numpy.mean(y)
    guess_ampl = 3 * numpy.std(y) / (2**0.5)
    guess_phase = 0
    error = 1e6

    # Need a good initial guess on phase to have the fit converge
    for phase in range(-180, 180, 10):
        est_ampl, est_phase, est_mean = optimize.leastsq(optimize_func, [guess_ampl, phase, guess_mean])[0]

        def fitted_func(t):
            return func(t, [est_ampl, est_phase, est_mean])

        this_error = numpy.sqrt(sum((fitted_func(x) - y) ** 2)) / len(y)
        if this_error < error:
            guess_phase = phase
            error = this_error

    # Now we use the best guess to make the fit
    est_ampl, est_phase, est_mean = optimize.leastsq(optimize_func, [guess_ampl, guess_phase, guess_mean])[0]

    def fitted_func(t):
        return func(t, [est_ampl, est_phase, est_mean])

    error = numpy.sqrt(sum((fitted_func(x) - y) ** 2)) / len(y)

    return fitted_func, est_ampl, est_phase, est_mean, error


def sine_fit(data, set_phases):
    """
    Sinusoidal fit

    Returns
        the data fit
        estimated std (amplitude)
        estimated phase (in degrees)
        estimated mean
    """

    def optimize_func(phases, x):
        return x[0] * numpy.cos((phases + x[1]) * numpy.pi / 180) + x[2]

    fitted_func, est_ampl, est_phase, est_mean, error = func_fit(set_phases, data, optimize_func)
    est_ampl = abs(est_ampl)

    return fitted_func, est_ampl, est_phase, est_mean, error


def curve_fit(data, set_phases, curve_x, curve_y):
    def optimize_func(phases, x):
        phases = phases + x[1]
        phases = phases % 360
        y = unwrap(curve_y)
        y = y - x[2]
        y *= x[0]
        y += x[2]
        return interp1d(curve_x, y)(phases)

    fitted_func, est_ampl, est_phase, est_mean, error = func_fit(set_phases, data, optimize_func)

    # Fix phase so it is returning where min is found
    xfine = numpy.arange(-180, 180, 0.01)
    i = numpy.where(fitted_func(xfine) == min(fitted_func(xfine)))[0][0]

    xfine2 = numpy.arange(0, 360, 0.01)
    yfine2 = interp1d(curve_x, unwrap(curve_y))(xfine2)
    i2 = numpy.where(yfine2 == min(yfine2))[0][0]
    if xfine2[i2] > 180:
        xfine2[i2] -= 360
    est_phase = xfine[i] - xfine2[i2]

    # Amplitude should be positive
    est_ampl = abs(est_ampl)

    # Mean fix
    est_mean = numpy.average(fitted_func(xfine))

    return fitted_func, est_ampl, est_phase, est_mean, error


def weighted_avg_and_std(values, weights, unwrap=None):
    """
    Return the weighted average and standard deviation.

    :param values: ndarray with values
    :param weights: ndarray with weights for each value

    :param unwrap: set to 360 for phase averaging
    """
    needs_unwrap = unwrap and max(abs(values)) > 0.45 * unwrap
    if needs_unwrap:
        values = values.copy()
        values += unwrap
        values = values % unwrap
    average = numpy.average(values, weights=weights)
    if needs_unwrap:
        if average > 0.5 * unwrap:
            average -= unwrap
    # Fast and numerically precise:
    variance = numpy.average((values - average) ** 2, weights=weights)
    return average, numpy.sqrt(variance)


def unwrap(x, period=360):
    """

    TODO Stability of this limit should be more carefully evaluated
    TODO this is unstable for big sine curves with few points, can we improve?
    """
    # Do not edit the original array..
    x = x.copy()
    x = numpy.array(x)

    # Check range limit
    diffs = abs(x[1:] - x[:-1])
    if numpy.median(diffs) > 0.2 * period:
        print("WARNING: unwrapping signal might fail")
    range_lim = period / 3
    for j in range(1, len(x)):
        breaker = 0
        while breaker < 10 and abs(x[j - 1] - x[j]) > range_lim:
            breaker += 1
            if x[j - 1] - x[j] < -range_lim:
                x[j] -= 360
            elif x[j - 1] - x[j] > range_lim:
                x[j] += 360
    return x


def interp1d(x, y, **kwargs):
    """
    Ensure the data set is in the range [0, 360]
    and return an 1D interpolation function
    """
    from scipy.interpolate import interp1d

    x = numpy.array(x) % 360
    y = numpy.array(y)  # % 360
    # interp1d requires unique values in x..
    x, unique_mask = numpy.unique(x, return_index=True)
    y = y[unique_mask]

    # The modulus will take out the end point value 360 (because it is same as 0)
    if 360 not in x:
        mask = x == 0
        x = numpy.append(x, 360)
        y = numpy.append(y, y[mask])
    return interp1d(x, y, **kwargs)
