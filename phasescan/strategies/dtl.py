import numpy
import sys
from ..abstracts import AbstractStrategy
import time
from .. import myepics


class AbstractDtlStrategy(AbstractStrategy):
    """
    The functionality that are common regardless of closed or open loop...
    """

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, seq):
        self._sequence = seq

    @property
    def parameters(self):
        return {"RF": ["Amplitude", "Phase"]}

    def get_settings(self, acc_node, nominal=False):
        settings = {}
        for parameter in self.get_all_parameters():
            settings[parameter] = self.get_setting(acc_node, parameter, nominal)
        return settings

    def get_monitor_data(self, monitors):
        """
        For each monitor, get the data and return

        This is the default, which is assuming it is a list of BPMs
        and the getPhaseAvg() function can be used to get phases.

        :param monitors: List of monitors to retrieve data from
        :return: A list of the measured data
        """
        phase_list_orig = [bpm.getPhaseAvg() for bpm in monitors]
        phase_list = [myepics.get(str(bpm.findChannel("phaseAvg").getId())) for bpm in monitors]
        print(phase_list, phase_list_orig)
        amp_list = [bpm.getAmpAvg() for bpm in monitors]
        return dict(phases=phase_list, amplitudes=amp_list)

    def _put(self, channel, value):
        myepics.put(channel, value)
        return myepics.get(channel)

    def analyse(self, variable, target, measured_data, simulated_data, axes=None):
        print("WARNING: Analysis not yet implemented")
        return {}


class DtlClosedStrategy(AbstractDtlStrategy):
    """
    This is a first strategy used for real with DTL tank 1.

    This is only intended for DTL tank 1 for now, in closed loop.

    Analysis is not implemented yet.
    """

    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized DTL closed loop strategy")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.dbg_plot = False
        self.monitors = []
        self._channels = {}
        self._handles = {"Amplitude": "git", "Phase": "cavPhaseWave", "PhaseAck": "cavPhaseAck"}
        self._sleep = 0.5  # wait time before acknowledging

    def get_channel(self, acc_node, param):
        aid = acc_node.getId()
        handle = self._handles[param]
        if aid not in self._channels:
            self._channels[aid] = {}
        if param not in self._channels[aid]:
            channel = acc_node.findChannel(handle)
            channel.connectAndWait()
            self._channels[aid][param] = channel
        return self._channels[aid][param]

    def get_channel_value(self, acc_node, param):
        channel = self.get_channel(acc_node, param)
        return channel.getArrDbl()

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if nominal:
            if parameter == "Amplitude":
                return acc_node.toCAFromCavAmpAvg(acc_node.getDfltCavAmp())
            elif parameter == "Phase":
                return acc_node.getDfltCavPhase()
        if parameter == "Phase":
            chan_name = str(self.get_channel(acc_node, parameter).channelName())
            self._put(f"{chan_name}.PROC", 1)
            if self._sleep:
                time.sleep(self._sleep)
        array = self.get_channel_value(acc_node, parameter)
        print(parameter, acc_node.getId(), numpy.average(array), len(array), type(array))
        return numpy.average(array)

    def apply_settings(self, acc_node, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Phase" in settings:
            array = self.get_channel_value(acc_node, "Phase")
            channel = self.get_channel(acc_node, "Phase")
            channel_ack = self.get_channel(acc_node, "PhaseAck")
            print("Phase before", acc_node.getId(), numpy.average(array))
            print(type(array))

            array_len = len(array)
            array = numpy.ones(array_len) * settings["Phase"]
            chan_name = channel.channelName()
            self._put(str(chan_name), array.tolist())
            if self._sleep:
                time.sleep(self._sleep)
            self._put(str(channel_ack.channelName()), 1)
        if "Amplitude" in settings:
            array = self.get_channel_value(acc_node, "Amplitude")
            print("WARNING: Not setting amplitude yet!")
            # print("Amplitude before", acc_node.getId(), numpy.average(array))
            # raise ValueError("TODO: Not setting amplitude correctly yet..")
            # acc_node.setCavAmp(settings["Amplitude"])
        return True
