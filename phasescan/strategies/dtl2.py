import numpy
from scipy.interpolate import interp1d
import sys
from ..abstracts import AbstractStrategy
import time
from .. import myepics


class AbstractDtlStrategy(AbstractStrategy):
    """
    The functionality that are common regardless of closed or open loop...
    """

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, seq):
        self._sequence = seq

    @property
    def parameters(self):
        return {"RF": ["Amplitude", "Phase"]}

    def get_settings(self, acc_node, nominal=False):
        settings = {}
        for parameter in self.get_all_parameters():
            settings[parameter] = self.get_setting(acc_node, parameter, nominal)
        return settings

    @property
    def _monitor_node_handles(self):
        handle_dict = {"BPM": ["phaseAvg", "amplitudeAvg", "xAvg", "yAvg"], "FC": ["currentAvg"], "BCM": ["currentAvg"], "RF": []}
        handle_dict["DTLTank"] = handle_dict["RF"]
        if self.include_waveforms:
            handle_dict["BPM"].append("phaseArray")
            handle_dict["FC"].append("currentArray")
            handle_dict["BCM"].append("currentArray")
            handle_dict["RF"].extend(["cavAmpArray", "cavPhaseArray", "cavFieldCtrl", "cavFwd1", "cavFwd2", "cavRefl1", "cavRefl2"])
        return handle_dict

    def get_monitor_data(self, monitors):
        """
        For each monitor, get the data and return

        This is the default, which is assuming it is a list of BPMs
        and the getPhaseAvg() function can be used to get phases.

        :param monitors: List of monitors to retrieve data from
        :return: A list of the measured data
        """
        data = {}
        for m in monitors:
            data[m] = {}
            t = m.getType()
            for h in self._monitor_node_handles[t]:
                chan = m.findChannel(h)
                if chan is not None:
                    data[m][h] = myepics.get(str(chan.getId()))
                else:
                    print(f"Could not connect to {h} of {m}")
                    data[m][h] = None

        return data

    def analyse(self, variable, target, measured_data, simulated_data, axes=None):
        # first array is weights, second is value
        setting_0 = [[], []]

        x = numpy.array(measured_data.monitors)
        y = []
        for monitor in measured_data.monitors:
            for ampl in measured_data.get_axis(monitor, 0):
                if ampl not in y:
                    y.append(ampl)
        y.sort()
        y = numpy.array(y)
        est_fit_errors = numpy.zeros((len(x), len(y)))

        # DBG
        from matplotlib import pyplot as plt

        for monitor in x:
            if str(monitor.getType()) != "BPM":
                continue
            # TODO for now we assume that all combinations of amplitude and phase were applied for the given cavity
            amplitudes = measured_data.get_axis(monitor, 0)
            phases = numpy.array(measured_data.get_axis(monitor, 1))
            if numpy.average(phases[1:] - phases[:-1]) > 20:
                print("WARNING: probably too high rfinterval for current method")
            sim_func = interp1d(simulated_data.set_points[monitor], simulated_data.data_set[monitor]["phases"], kind="cubic")

            lt = None
            for amplitude in amplitudes:
                data = measured_data.get_data_for(monitor, axes=[[amplitude], phases])[0]
                fit_function, shifted_data, est_ampl, est_phase, est_mean, error = self._sine_fit(data, phases)
                # TODO Weight equal to the amplitude of the sinusoidal fit:
                # This should be improved to rather weight by the quality of the fit
                # TODO: weight should be correlated with the BPM amplitude as well (to ignore debunched beam reading)
                if not self.fixed_phase:
                    setting_0[0].append(est_ampl)
                    this_phase = (target["Phase"] - est_phase) % 360
                    if this_phase > 180:
                        this_phase -= 360
                    setting_0[1].append(this_phase)

                if self.fixed_phase:
                    # We assume that the current phase setting is "perfect"
                    est_phase = target["Phase"] - self.get_setting(variable, "Phase")
                new_phase = phases + target["Phase"] + est_phase
                new_phase = new_phase % 360
                data_for_ampl_fit = shifted_data - sim_func(new_phase)
                if self.dbg_plot and monitor in x[:2] and amplitude in amplitudes[-1:]:
                    plt.figure()
                    plt.title(f"{monitor.getId()}")
                    plt.plot(new_phase, shifted_data - numpy.average(shifted_data), "x", label="meas")
                    plt.plot(new_phase, sim_func(new_phase), "o", label="sim")
                    # plt.plot(new_phase, data_for_ampl_fit, ".-", label="diff")
                    plt.plot(simulated_data.set_points[monitor], simulated_data.data_set[monitor]["phases"])
                    plt.legend()
                data_for_ampl_fit -= numpy.average(data_for_ampl_fit)
                # TODO should be xhi-square rather
                est_fit_errors[numpy.where(x == monitor)[0], numpy.where(y == amplitude)[0]] = numpy.sum(abs(data_for_ampl_fit))

                if axes is not None:
                    fit_x = numpy.arange(min(phases), max(phases), 0.5)
                    # Plot the curves:
                    if lt is None:
                        lt = axes[0].plot(phases, shifted_data, ".-", label=f"{monitor}")[0]
                    else:
                        axes[0].plot(phases, shifted_data, ".-", linestyle=lt.get_linestyle(), color=lt.get_color())

                    axes[0].plot(fit_x, fit_function(fit_x), color=lt.get_color())
                    axes[0].plot(this_phase, fit_function(this_phase), "x", color=lt.get_color())

                    weights = numpy.array(setting_0[0])
                    positions = numpy.array(setting_0[1])
                    axes[1].errorbar(numpy.arange(len(setting_0[0])) + 1, positions, yerr=1 / numpy.sqrt(weights))
                    axes[2].hist(positions, max(len(positions) // 3, 6))

        ampl_best_fit = numpy.zeros(len(x))
        ampl_weight = numpy.zeros(len(x))
        for i in range(len(est_fit_errors)):
            i_ampl = numpy.where(est_fit_errors[i] == min(est_fit_errors[i]))[0][0]
            ampl_best_fit[i] = amplitudes[i_ampl]  # TODO better to do a proper fit
            ampl_weight[i] = 1  # TODO replace by quality of BPM data

        if self.dbg_plot:
            plt.figure()
            for i in range(len(est_fit_errors)):
                plt.plot(amplitudes, est_fit_errors[i] / max(est_fit_errors[i]), "x--", label=f"BPM {i+2}")
            plt.xlabel("BPM index")
            plt.ylabel("Diff to simulated amplitude")
            plt.legend()
            plt.show()
            plt.close()

        if self.fixed_phase:
            new_phase = self.get_setting(variable, "Phase")
        else:
            new_phase = (sum(numpy.array(setting_0[0]) * numpy.array(setting_0[1])) / sum(setting_0[0])) % 360
            if new_phase > 180:
                new_phase -= 360
        new_amplitude = numpy.average(ampl_best_fit)
        new_settings = dict(Phase=new_phase, Amplitude=new_amplitude)
        return new_settings

    def _sine_fit(self, data, set_phases):
        """
        Sinusoidal fit

        Returns
            the data fit
            estimated std (amplitude)
            estimated phase (in degrees)
            estimated mean
        """
        from scipy import optimize

        # Do not edit the original array..
        data = data.copy()

        # Calculate range limit (to detect when a "360 jump" occurs)
        # TODO Stability of this limit should be more carefully evaluated
        diffs = abs(data[1:] - data[:-1])
        range_lim = numpy.average(diffs) * 2

        # TODO this is unstable for big sine curves with few points, can we improve?
        for j in range(1, len(data)):
            breaker = 0
            while breaker < 10 and abs(data[j - 1] - data[j]) > range_lim:
                breaker += 1
                if data[j - 1] - data[j] < -range_lim:
                    data[j] -= 360
                elif data[j - 1] - data[j] > range_lim:
                    data[j] += 360
        guess_mean = numpy.mean(data)

        guess_ampl = 3 * numpy.std(data) / (2**0.5) / (2**0.5)
        guess_phase = 0

        def optimize_func(x):
            return x[0] * numpy.cos((set_phases + x[1]) * numpy.pi / 180) + x[2] - data

        est_ampl, est_phase, est_mean = optimize.leastsq(optimize_func, [guess_ampl, guess_phase, guess_mean])[0]

        if est_ampl < 0:
            # Force positive amplitude:
            est_ampl *= -1
        else:
            # Phase is 180 deg different from what we want
            # Only when amplitude is "correct"
            est_phase += 180

        def data_fit(t):
            # negative amplitude because we fit "oppositely"
            return -est_ampl * numpy.cos((t + est_phase) * numpy.pi / 180) + est_mean

        error = numpy.sqrt(sum((data_fit(set_phases) - data) ** 2)) / len(data)

        return data_fit, data, est_ampl, est_phase, est_mean, error


class DtlOpenStrategy(AbstractDtlStrategy):
    """
    This is only intended for DTL tank 1, in open loop mode.

    This is essentially a copy of MEBT Buncher code, and has not been tested on DTL yet

    Do not use analysis code for now.
    """

    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized DTL open loop strategy 2")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.dbg_plot = False
        self.monitors = []
        self._channels = {}
        self.include_waveforms = True

    def _chan_base(self, acc_node):
        channel_name = str(acc_node.findChannel("cavAmpSet").getId())
        return ":".join(channel_name.split(":")[:-1])

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if nominal:
            if parameter == "Amplitude":
                return acc_node.toCAFromCavAmpAvg(acc_node.getDfltCavAmp())
            elif parameter == "Phase":
                return acc_node.getDfltCavPhase()
        value = None
        if parameter == "Phase":
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenPhase"
            value = myepics.get(chan_name)
        if parameter == "Amplitude":
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenP"
            value = myepics.get(chan_name)
        return value

    def apply_settings(self, acc_node, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Phase" in settings:
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenPhase"
            print("-- Setting phase", chan_name, settings["Phase"])
            myepics.put(chan_name, settings["Phase"])
        if "Amplitude" in settings:
            if settings["Amplitude"] > 0.16:
                raise ValueError("You tried to set amplitude of {settings['Amplitude'] * 1000} kV, max allowed is 160 kV")
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenP"
            print("-- Setting amplitude", chan_name, settings["Amplitude"])
            myepics.put(chan_name, settings["Amplitude"])
        return True


class DtlClosedStrategy(AbstractDtlStrategy):
    """
    This is a first strategy used for real.

    This is only intended for DTL tank 1, in closed loop.

    This is essentially a copy of the strategy for MEBT Bunchers in closed loop.

    Included analysis code will not get you anywhere for now.
    """

    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized Dtl closed loop strategy 2")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.dbg_plot = False
        self.monitors = []
        self._channels = {}
        self.include_waveforms = True
        self._handles = {"Amplitude": "cavAmpWave", "Phase": "cavPhaseWave", "PhaseAck": "cavPhaseAck"}
        self._sleep = 0.5  # wait time before acknowledging

    def get_channel(self, acc_node, param):
        aid = acc_node.getId()
        handle = self._handles[param]
        if aid not in self._channels:
            self._channels[aid] = {}
        if param not in self._channels[aid]:
            channel = acc_node.findChannel(handle)
            channel.connectAndWait()
            self._channels[aid][param] = channel
        return self._channels[aid][param]

    def get_channel_value(self, acc_node, param):
        channel = self.get_channel(acc_node, param)
        return channel.getArrDbl()

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if nominal:
            if parameter == "Amplitude":
                return acc_node.getDfltCavAmp()
            elif parameter == "Phase":
                return acc_node.getDfltCavPhase()
        if parameter == "Phase":
            chan_name = str(self.get_channel(acc_node, parameter).channelName())
            myepics.put(f"{chan_name}.PROC", 1)
            if self._sleep:
                time.sleep(self._sleep)
            array = self.get_channel_value(acc_node, parameter)
            return numpy.average(array) * 180 / numpy.pi
        elif parameter == "Amplitude":
            return acc_node.getCavAmpSetPoint()
        raise ValueError(f"Wrong parameter: {parameter}")

    def _apply_value_in_steps(self, value, set_func, get_func, max_step, sleep=2):
        delta = value - get_func()
        for i in range(int(abs(delta) / max_step)):
            if delta > 0:
                set_func(get_func() + max_step)
            else:
                set_func(get_func() - max_step)
            time.sleep(sleep)
            delta = value - get_func()
        set_func(value)

    def apply_settings(self, acc_node, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Amplitude" in settings:
            if settings["Amplitude"] > 3.3001:
                raise ValueError(f"You tried to set amplitude of {settings['Amplitude']} MV, max allowed is 3.3 MV")
            delta = settings["Amplitude"] - acc_node.getCavAmpSetPoint()
            if abs(delta) > 0.001:  # Otherwise we presumably just want same set point as last time..
                print(f" -- Setting amplitude, {acc_node.getId()}: {acc_node.getCavAmpSetPoint()} -> {settings['Amplitude']}")
                self._apply_value_in_steps(settings["Amplitude"], acc_node.setCavAmp, acc_node.getCavAmpSetPoint, 0.1)  # 10 kV max
                print("--- New amplitude:", acc_node.getId(), acc_node.getCavAmpSetPoint())
                if self._sleep:
                    time.sleep(self._sleep)
        if "Phase" in settings:
            new_phase = settings["Phase"] / 180 * numpy.pi
            if abs(new_phase) > numpy.pi:
                new_phase = ((new_phase + numpy.pi) % (2 * numpy.pi)) - numpy.pi
            print(f" -- Setting phase, {acc_node.getId()}: {acc_node.getCavPhaseSetPoint()} -> {new_phase}")
            acc_node.setCavPhase(new_phase)
        return True
