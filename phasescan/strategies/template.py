from abstracts import AbstractStrategy


class DtlStrategy(AbstractStrategy):
    def __init__(self, sequence):
        self._sequence = sequence

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, sequence):
        self._sequence = sequence

    @property
    def parameters(self):
        return {"RF": ["Amplitude", "Phase"]}

    def get_setting(self, acc_node, parameter, nominal=False):
        if parameter == "Amplitude":
            if nominal:
                return acc_node.toCAFromCavAmpAvg(acc_node.getDfltCavAmp())
            else:
                return acc_node.getCavAmpSetPoint()
        elif parameter == "Phase":
            if nominal:
                return acc_node.getDfltCavPhase()
            else:
                return acc_node.getCavPhaseSetPoint()

    def apply_settings(self, variable, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Amplitude" in settings:
            variable.setCavAmp(settings["Amplitude"])
        if "Phase" in settings:
            variable.setCavPhase(settings["Phase"])
        return True

    def analyse(self, acc_node, target, measured_data, simulated_data, axes=None):
        # Here you insert your own analysis of the data obtained
        # For now we just pull the design settings and suggest those.
        new_phase = acc_node.getDfltCavPhase()
        new_amplitude = acc_node.toCAFromCavAmpAvg(acc_node.getDfltCavAmp())
        new_settings = dict(Phase=new_phase, Amplitude=new_amplitude)
        return new_settings
