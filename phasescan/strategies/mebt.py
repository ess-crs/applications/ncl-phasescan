import sys
import time
from ..abstracts import AbstractStrategy
from ..fitting import individual_curves


class MebtClosedStrategy(AbstractStrategy):
    """
    This is a first strategy used for real.

    This is only intended for MEBT bunchers, in closed loop.

    This is using arrays to set phase and amplitude.
    It makes the assumption that these do not exist in the OpenXAL model,
    so PV names are auto-translated based on buncher name.

    This only works in closed loop
    """

    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized MEBT closed loop strategy")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.dbg_plot = False
        self.monitors = []
        self._channels = {}
        self._max_allowed_amp = 0.16
        self.include_waveforms = True
        self.debug_no_scanning = False
        self._handles = {"Amplitude": "cavAmpWave", "Phase": "cavPhaseWave", "PhaseAck": "cavPhaseAck"}
        self._sleep = 0.5  # wait time before acknowledging
        self.analyser = individual_curves.Analyser(self.fixed_phase, self.dbg_plot)

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, seq):
        self._sequence = seq

    @property
    def parameters(self):
        return {"RF": ["Amplitude", "Phase"]}

    def get_settings(self, acc_node, nominal=False):
        settings = {}
        for parameter in self.get_all_parameters():
            settings[parameter] = self.get_setting(acc_node, parameter, nominal)
        return settings

    @property
    def _monitor_node_handles(self):
        handle_dict = {"BPM": ["phaseAvg", "amplitudeAvg", "xAvg", "yAvg"], "FC": ["currentAvg"], "BCM": ["currentAvg"], "RF": []}
        if self.include_waveforms:
            handle_dict["BPM"] += ["phaseArray", "sigROIStart", "sigROIEnd"]
            handle_dict["FC"].append("currentArray")
            handle_dict["BCM"].append("currentArray")
            handle_dict["RF"].extend(["cavAmpArray", "cavPhaseArray"])
        return handle_dict

    def analyse(self, variable, target, measured_data, simulated_data):
        self.analyser.dbg_plot = self.dbg_plot
        self.analyser.fixed_phase = self.fixed_phase
        return self.analyser.analyse(variable, target, measured_data, simulated_data)

    def get_channel(self, acc_node, param):
        aid = acc_node.getId()
        handle = self._handles[param]
        if aid not in self._channels:
            self._channels[aid] = {}
        if param not in self._channels[aid]:
            channel = acc_node.findChannel(handle)
            channel.connectAndWait()
            self._channels[aid][param] = channel
        return self._channels[aid][param]

    def get_channel_value(self, acc_node, param):
        channel = self.get_channel(acc_node, param)
        return channel.getArrDbl()

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if nominal:
            if parameter == "Amplitude":
                return acc_node.getDfltCavAmp()
            elif parameter == "Phase":
                return acc_node.getDfltCavPhase()
        if parameter == "Phase":
            return acc_node.getCavPhaseSetPoint()
        elif parameter == "Amplitude":
            return acc_node.getCavAmpSetPoint()
        raise ValueError(f"Wrong parameter: {parameter}")

    def _apply_value_in_steps(self, value, set_func, get_func, max_step, sleep=2):
        delta = value - get_func()
        for i in range(int(abs(delta) / max_step)):
            if delta > 0:
                set_func(get_func() + max_step)
            else:
                set_func(get_func() - max_step)
            time.sleep(sleep)
            delta = value - get_func()
        set_func(value)

    def apply_settings(self, acc_node, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Amplitude" in settings:
            if settings["Amplitude"] > self._max_allowed_amp:
                raise ValueError(
                    f"You tried to set amplitude of {settings['Amplitude'] * 1000} kV, max allowed is {self._max_allowed_amp*1000} kV"
                )
            current_amplitude = acc_node.getCavAmpSetPoint()
            if abs(current_amplitude - settings["Amplitude"]) > 1e-5:  # less than 0.01 kV, probably just want to keep same set point?
                print(f" -- Setting amplitude, {acc_node.getId()}: {acc_node.getCavAmpSetPoint()} -> {settings['Amplitude']}")
                if True:  # if True, ramp amplitude slowly.. should not be necessary!
                    self._apply_value_in_steps(settings["Amplitude"], acc_node.setCavAmp, acc_node.getCavAmpSetPoint, 0.01)  # 10 kV max
                if self._sleep:
                    time.sleep(self._sleep)
        if "Phase" in settings:
            new_phase = settings["Phase"]
            if abs(new_phase) > 180:
                new_phase = ((new_phase + 180) % 360) - 180

            print(f" -- Setting phase, {acc_node.getId()}: {acc_node.getCavPhaseSetPoint()} -> {new_phase}")
            acc_node.setCavPhase(new_phase)
        return True
