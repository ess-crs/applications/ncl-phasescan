import sys
from ..abstracts import AbstractStrategy
import time
from .. import myepics
from ..fitting import mebt_bunchers2


class AbstractMebtStrategy(AbstractStrategy):
    """
    The functionality that are common regardless of closed or open loop...
    """

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, seq):
        self._sequence = seq

    @property
    def parameters(self):
        return {"RF": ["Amplitude", "Phase"]}

    def get_settings(self, acc_node, nominal=False):
        settings = {}
        for parameter in self.get_all_parameters():
            settings[parameter] = self.get_setting(acc_node, parameter, nominal)
        return settings

    @property
    def _monitor_node_handles(self):
        handle_dict = {"BPM": ["phaseAvg", "amplitudeAvg", "xAvg", "yAvg"], "FC": ["currentAvg"], "BCM": ["currentAvg"], "RF": []}
        if self.include_waveforms:
            handle_dict["BPM"].append("phaseArray")
            handle_dict["FC"].append("currentArray")
            handle_dict["BCM"].append("currentArray")
            handle_dict["RF"].extend(["cavAmpArray", "cavPhaseArray"])
        return handle_dict

    def get_monitor_data(self, monitors):
        """
        For each monitor, get the data and return

        This is the default, which is assuming it is a list of BPMs
        and the getPhaseAvg() function can be used to get phases.

        :param monitors: List of monitors to retrieve data from
        :return: A list of the measured data
        """
        data = {}
        for m in monitors:
            data[m] = {}
            t = m.getType()
            for h in self._monitor_node_handles[t]:
                chan = m.findChannel(h)
                if chan is not None:
                    data[m][h] = myepics.get(str(chan.getId()))
                else:
                    print(f"Could not connect to {h} of {m}")
                    data[m][h] = None

        return data


class MebtOpenStrategy(AbstractMebtStrategy):
    """
    This is a firster strategy used for real.

    This is only intended for MEBT bunchers, in open loop mode.


    This only works in open loop
    """

    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized MEBT open loop strategy")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.dbg_plot = False
        self.monitors = []
        self._channels = {}
        self.include_waveforms = True

    def _chan_base(self, acc_node):
        channel_name = str(acc_node.findChannel("cavAmpSet").getId())
        return ":".join(channel_name.split(":")[:-1])

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if nominal:
            if parameter == "Amplitude":
                return acc_node.toCAFromCavAmpAvg(acc_node.getDfltCavAmp())
            elif parameter == "Phase":
                return acc_node.getDfltCavPhase()
        value = None
        if parameter == "Phase":
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenPhase"
            value = myepics.get(chan_name)
        if parameter == "Amplitude":
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenP"
            value = myepics.get(chan_name)
        return value

    def apply_settings(self, acc_node, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Phase" in settings:
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenPhase"
            print("-- Setting phase", chan_name, settings["Phase"])
            myepics.put(chan_name, settings["Phase"])
        if "Amplitude" in settings:
            if settings["Amplitude"] > 0.16:
                raise ValueError("You tried to set amplitude of {settings['Amplitude'] * 1000} kV, max allowed is 160 kV")
            chan_name = f"{self._chan_base(acc_node)}:FFPulseGenP"
            print("-- Setting amplitude", chan_name, settings["Amplitude"])
            myepics.put(chan_name, settings["Amplitude"])
        return True

    def analyse(self, variable, target, measured_data, simulated_data):
        analyser = mebt_bunchers2.Analyser(self.fixed_phase)
        return analyser.analyse(variable, target, measured_data, simulated_data)


class MebtClosedStrategy(AbstractMebtStrategy):
    """
    This is a first strategy used for real.

    This is only intended for MEBT bunchers, in closed loop.

    This is using arrays to set phase and amplitude.
    It makes the assumption that these do not exist in the OpenXAL model,
    so PV names are auto-translated based on buncher name.

    This only works in closed loop
    """

    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized MEBT closed loop strategy")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.dbg_plot = False
        self.monitors = []
        self._channels = {}
        self.include_waveforms = True
        self.debug_no_scanning = False
        self._sleep = 0.0  # wait time before acknowledging

    def get_channel_id(self, acc_node, handle):
        """
        TODO: Probably excessive to use this..
        :param acc_node:
        :param handle:
        :return:
        """
        return acc_node.findChannel(handle).getId()

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if nominal:
            if parameter == "Amplitude":
                return acc_node.getDfltCavAmp()
            elif parameter == "Phase":
                return acc_node.getDfltCavPhase()
        if parameter == "Phase":
            pvget = self.get_channel_id(acc_node, "cavPhaseAvg")
            return myepics.get(pvget)
        elif parameter == "Amplitude":
            pvget = self.get_channel_id(acc_node, "cavAmpAvg")
            return myepics.get(pvget)
        raise ValueError(f"Wrong parameter: {parameter}")

    def apply_settings(self, acc_node, settings, should_have_all=False):
        """
        getCavAmpSetPoint() and getCavPhaseSetPoint() are quite slow for some reason??
        """
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Phase" in settings:
            pvget = self.get_channel_id(acc_node, "cavPhaseAvg")
            pvput = self.get_channel_id(acc_node, "cavPhaseSet")
            current_phase = pvget.get()
            new_phase = settings["Phase"]
            if abs(new_phase) > 180:
                new_phase = ((new_phase + 180) % 360) - 180
            if abs(current_phase - new_phase) > 1e-3:  # less than 0.001 degree, probably just want to keep same set point?
                print(f" -- Setting phase, {acc_node.getId()}: {current_phase} -> {new_phase}")
                if False:  # if True, ramp phase slowly..
                    delta = new_phase - current_phase
                    for i in range(int(abs(delta) / 0.1)):
                        if delta > 0:
                            myepics.put(pvput, myepics.get(pvget) + 0.1)
                        else:
                            myepics.put(pvput, myepics.get(pvget) - 0.1)
                        time.sleep(1)
                        delta = new_phase - pvget.get()
                pvput.put(new_phase)
        if "Amplitude" in settings:
            if settings["Amplitude"] > 0.16:
                raise ValueError(f"You tried to set amplitude of {settings['Amplitude'] * 1000} kV, max allowed is 160 kV")
            pvget = self.get_channel_id(acc_node, "cavAmpAvg")
            pvput = self.get_channel_id(acc_node, "cavAmpSet")
            current_amplitude = pvget.get() / 1000
            if abs(current_amplitude - settings["Amplitude"]) < 1e-5:  # less than 0.01 kV, probably just want to keep same set point?
                return True
            print(f" -- Setting amplitude, {acc_node.getId()}: {current_amplitude} -> {settings['Amplitude']}")
            if True:  # if True, ramp amplitude slowly.. should not be necessary!
                delta = settings["Amplitude"] - current_amplitude
                for i in range(int(abs(delta) / 0.1)):
                    if delta > 0:
                        myepics.put(pvput, myepics.get(pvget) + 0.1)
                    else:
                        myepics.put(pvput, myepics.get(pvget) - 0.1)
                    time.sleep(2)
                    delta = settings["Amplitude"] - (pvget.get())
            myepics.put(pvput, settings["Amplitude"])
            if self._sleep:
                time.sleep(self._sleep)
        return True
