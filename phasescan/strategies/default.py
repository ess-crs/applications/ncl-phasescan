import sys
from ..abstracts import AbstractStrategy
from ..fitting import individual_curves


class DefaultStrategy(AbstractStrategy):
    def __init__(self, sequence):
        if sys.flags.debug:
            print("Initialized default strategy")
        self._sequence = sequence
        # In case of multiple variables, there will correspondingly be multiple lists of parameters returned (for each variable)
        self.fixed_phase = False
        self.include_waveforms = False
        self.dbg_plot = False
        self.analyser = individual_curves.Analyser(self.fixed_phase, self.dbg_plot)

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, seq):
        self._sequence = seq

    @property
    def parameters(self):
        return {"RF": ["Amplitude", "Phase"]}

    def get_setting(self, acc_node, parameter, nominal=False):
        self._check_valid_parameters(parameter)
        if parameter == "Amplitude":
            if nominal:
                return acc_node.toCAFromCavAmpAvg(acc_node.getDfltCavAmp())
            else:
                return acc_node.getCavAmpSetPoint()
        elif parameter == "Phase":
            if nominal:
                return acc_node.getDfltCavPhase()
            else:
                return acc_node.getCavPhaseSetPoint()

    def apply_settings(self, variable, settings, should_have_all=False):
        self._check_valid_parameters(settings, should_have_all=should_have_all)
        if "Amplitude" in settings:
            variable.setCavAmp(settings["Amplitude"])
        if "Phase" in settings:
            variable.setCavPhase(settings["Phase"])
        return True

    def analyse(self, variable, target, measured_data, simulated_data):
        self.analyser.dbg_plot = self.dbg_plot
        self.analyser.fixed_phase = self.fixed_phase
        return self.analyser.analyse(variable, target, measured_data, simulated_data)
